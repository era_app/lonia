//
//  CachedArrays.swift
//  Lonia
//
//  Created by SSR Lab on 16.03.22.
//

import Foundation
import UIKit

    //ID
class SavedBrifcase: Codable {
    var dateOfCreation: Date = Date()
    var inspectorSource: String = ""
    var port: String = ""
    var inspectorName: String = ""
    var inspectorType: String = ""
    var vessel: String = ""
    var questionaries: String = ""
    var notes: [String] = []
    var categories: [String] = []
    var questions: [QuestionsSave] = []
    var isSended: Bool = false
    
    init(inspector: String, inspectorName: String, inspectorType: String, vessel: String, category: String, question: [QuestionsSave], isAnswered: Bool, port: String, dateOfCreation: Date, notes: [String], categories: [String], isSended: Bool) {
        self.inspectorSource = inspector
        self.inspectorName = inspectorName
        self.inspectorType = inspectorType
        self.vessel = vessel
        self.questionaries = category
        self.port = port
        self.dateOfCreation = dateOfCreation
        self.notes = notes
        self.categories = categories
        self.isSended = isSended
    }
    
}

class QuestionsSave: Codable {
    var dateOfInspection: String = ""
    var questionID: String = ""
    var questionInformation: String = ""
    var answer: String = ""
    var questionCode = ""
    var question = ""
    var comment = ""
    var categoryID = ""
    var categoryName = ""
    var origin = ""
    var categoryNewID = ""
    var isAnswered = false
    var significance = ""
    var images: [String] = []
    
    init(questionCode: String, question: String, comment: String, categoryID: String, origin: String, categoryNewID: String, isAnswered: Bool, answer: String, dateOfInspection: String, images: [String], questionID: String, categoryName: String, questionInformation: String, significant: String) {
        self.question = question
        self.categoryID = categoryID
        self.comment = comment
        self.questionCode = questionCode
        self.categoryNewID = categoryNewID
        self.isAnswered = isAnswered
        self.origin = origin
        self.answer = answer
        self.dateOfInspection = dateOfInspection
        self.images = images
        self.questionID = questionID
        self.categoryName = categoryName
        self.questionInformation = questionInformation
        self.significance = significant
    }
}

class CategorySectionCellData {
    var opened: Bool!
    var title: String!
    var sectionsData: [QuestionsSave] = []
    
    init(opened: Bool, title: String, sectionsData: [QuestionsSave]) {
        self.opened = opened
        self.title = title
        self.sectionsData = sectionsData
    }
}


class SelectedQuestions {
    var section: Int
    var row: Int
    var question: String
    var answer: String
    var dateOfInspection: String
    var comment: String
    var sifnificant: String
    
    init(section: Int, row: Int, question: String, answer: String, dateOfInspection: String, comment: String, significant: String) {
        self.section = section
        self.row = row
        self.question = question
        self.answer = answer
        self.dateOfInspection = dateOfInspection
        self.comment = comment
        self.sifnificant = significant
    }
}
