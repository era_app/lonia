//
//  Extensions.swift
//  Lonia
//
//  Created by SSR Lab on 16.03.22.
//

import Foundation
import UIKit

extension UITableView {
    func registerCell(_ cellClass: AnyClass) {
        let nib = UINib(nibName: String(describing: cellClass.self), bundle: nil)
        self.register(nib, forCellReuseIdentifier: String(describing: cellClass.self))
    }
    
    func setupDelegateData(_ controller: UIViewController) {
        self.delegate = controller as? UITableViewDelegate
        self.dataSource = controller as? UITableViewDataSource
        self.tableFooterView = UIView()
    }
}

extension UIViewController {
    @objc func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:    #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    public func addActionSheetForiPad(actionSheet: UIAlertController) {
        if let popoverPresentationController = actionSheet.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverPresentationController.permittedArrowDirections = []
        }
    }
    
    
}

struct ImageCompressor {
    static func compress(image: UIImage, maxByte: Int,
                         completion: @escaping (UIImage?) -> ()) {
        DispatchQueue.global(qos: .userInitiated).async {
            guard let currentImageSize = image.jpegData(compressionQuality: 1.0)?.count else {
                return completion(nil)
            }
        
            var iterationImage: UIImage? = image
            var iterationImageSize = currentImageSize
            var iterationCompression: CGFloat = 1.0
        
            while iterationImageSize > maxByte && iterationCompression > 0.01 {
                let percantageDecrease = getPercantageToDecreaseTo(forDataCount: iterationImageSize)
            
                let canvasSize = CGSize(width: image.size.width * iterationCompression,
                                        height: image.size.height * iterationCompression)
                UIGraphicsBeginImageContextWithOptions(canvasSize, false, image.scale)
                defer { UIGraphicsEndImageContext() }
                image.draw(in: CGRect(origin: .zero, size: canvasSize))
                iterationImage = UIGraphicsGetImageFromCurrentImageContext()
            
                guard let newImageSize = iterationImage?.jpegData(compressionQuality: 1.0)?.count else {
                    return completion(nil)
                }
                iterationImageSize = newImageSize
                iterationCompression -= percantageDecrease
            }
            completion(iterationImage)
        }
    }

    private static func getPercantageToDecreaseTo(forDataCount dataCount: Int) -> CGFloat {
        switch dataCount {
        case 0..<3000000: return 0.05
        case 3000000..<10000000: return 0.1
        default: return 0.2
        }
    }
}




extension UITextField {
    func setInputViewDatePicker(target: Any, selector: Selector) {
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 271))//1
        datePicker.datePickerMode = .date //2
        datePicker.timeZone = .current
        // iOS 14 and above
        if #available(iOS 14, *) {// Added condition for iOS 14
          datePicker.preferredDatePickerStyle = .wheels
          datePicker.sizeToFit()
        }
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    class func loadFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T 
    }
    

    
}

extension UIViewController {
    func setupNavigationBar(view: UIViewController) {
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.black]
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.backgroundColor = .white
        view.navigationController?.navigationBar.isHidden = false
        view.navigationController?.navigationBar.standardAppearance = navBarAppearance
        view.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        view.navigationController?.navigationBar.backgroundColor = .white
        view.navigationController?.navigationBar.isTranslucent = false
    }
    
    func addBorder(view: UIView) {
        UIView.animate(withDuration: 0.3) {
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.systemRed.cgColor
        }
    }
}




extension Sequence where Element: Hashable {
    func uniqued() -> [Element] {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
}

extension UIViewController {
    func getCategoryName (value: Int) -> String {
        if value == 1859 {
            return "Certification and Documentation"
        }
        if value == 1860 {
            return "Certification and Documentation"
        }
        if value == 1861 {
            return "Certification and Documentation"
        }
        if value == 1862 {
            return "Certification and Documentation"
        }
        if value == 1863 {
            return "Certification and Documentation"
        }
        if value == 1864 {
            return "Certification and Documentation"
        }
        if value == 1865 {
            return "Certification and Documentation"
        }
        if value == 1866 {
            return "Certification and Documentation"
        }
        if value == 1867 {
            return "Certification and Documentation"
        }
        if value == 1868 {
            return "Certification and Documentation"
        }
        if value == 1869 {
            return "Certification and Documentation"
        }
        if value == 1870 {
            return "Certification and Documentation"
        }
        if value == 1871 {
            return "Certification and Documentation"
        }
        if value == 1872 {
            return "Crew managment"
        }
        if value == 1873 {
            return "Crew managment"
        }
        if value == 1874 {
            return "Crew managment"
        }
        if value == 1875 {
            return "Crew managment"
        }
        if value == 1876 {
            return "Crew managment"
        }
        if value == 1877 {
            return "Crew managment"
        }
        if value == 1878 {
            return "Crew managment"
        }
        if value == 1879 {
            return "Crew managment"
        }
        if value == 1880 {
            return "Crew managment"
        }
        if value == 1881 {
            return "Crew managment"
        }
        if value == 1882 {
            return "Crew managment"
        }
        if value == 1883 {
            return "Crew managment"
        }
        if value == 1884 {
            return "Crew managment"
        }
        if value == 1885 {
            return "Crew managment"
        }
        if value == 1886 {
            return "Crew managment"
        }
        if value == 1888 {
            return "Crew managment"
        }
        if value == 1889 {
            return "Crew managment"
        }
        if value == 1890 {
            return "Crew managment"
        }
        if value == 1891 {
            return "Fire fighting Equipment"
        }
        if value == 1892 {
            return "Access"
        }
        if value == 2027 {
            return "Access"
        }
        if value == 2029 {
            return "Access"
        }
        if value == 2032 {
            return "Access"
        }
        if value == 2033 {
            return "Access"
        }
        if value == 2034 {
            return "Access"
        }
        if value == 2035 {
            return "Access"
        }
        if value == 2050 {
            return "Access"
        }
        if value == 2051 {
            return "Access"
        }
        if value == 2052 {
            return "Access"
        }
        if value == 2053 {
            return "Access"
        }
        if value == 2054 {
            return "Access"
        }
        if value == 2055 {
            return "Access"
        }
        if value == 2056 {
            return "Access"
        }
        if value == 2057 {
            return "Access"
        }
        if value == 2058 {
            return "Access"
        }
        if value == 2059 {
            return "Access"
        }
        if value == 2060 {
            return "Access"
        }
        if value == 2061 {
            return "Access"
        }
        if value == 2062 {
            return "Access"
        }
        if value == 2063 {
            return "Access"
        }
        if value == 2064 {
            return "Access"
        }
        if value == 2065 {
            return "Access"
        }
        if value == 2066 {
            return "Access"
        }
        if value == 2067 {
            return "Access"
        }
        if value == 2068 {
            return "Access"
        }
        if value == 2069 {
            return "Access"
        }
        if value ==  2070 {
            return "Ullaging, Sampling and Closed Operations"
        }
        if value ==  2071 {
            return "Ullaging, Sampling and Closed Operations"
        }
        if value ==  2072 {
            return "Mooring Equipment Documentation and management"
        }
        if value ==  2073 {
            return "Mooring Equipment Documentation and management"
        }
        if value ==  2074 {
            return "Mooring Equipment Documentation and management"
        }
        if value ==  2075 {
            return "Mooring Equipment Documentation and management"
        }
        if value ==  2076 {
            return "Safety Management and the Operator’s Procedures Manuals"
        }
        if value ==  2077 {
            return "Safety Management and the Operator’s Procedures Manuals"
        }
        if value == 2078 {
            return "Survey and Repair History"
        }
        if value == 2079 {
            return "Anti-Pollution  "
        }
        if value == 2080 {
            return "Structure"
        }
        if value ==  2081 {
            return "Crew Qualifications"
        }
        if value ==  2082 {
            return "Drug and alcohol policy"
        }
        if value ==  2083 {
            return "Drug and alcohol policy"
        }
        if value ==  2084 {
           return  "Drug and alcohol policy"
        }
         if value == 2085 {
            return "Navigation equipment"
        }
        if value ==  2086 {
            return  "Communications"
        }
        if value ==  2087 {
            return  "Communications"
        }
        if value == 2088 {
            return "Safety Management"
        }
        if value ==  2090 {
           return  "Hot work procedures"
        }
        if value == 2092 {
            return "Sample Arrangements"
        }
         
        if value == 2093 {
            return "Cargo Operations and Deck Area Pollution Prevention"
        }
        if value == 2094 {
            return "Ballast Water Management"
        }
        if value == 2095 {
            return "Ballast Water Management"
        }
        if value == 2096 {
            return "Stability and Cargo Loading limitations"
        }
        if value ==  2097 {
           return "Cargo Operations and Related Safety Management"
        }
        if value ==  2098 {
            return "Ullaging Sampling and Closed Operations"
        }
        if value ==  2120 {
            return "Ullaging Sampling and Closed Operations"
        }
        if value ==  2123 {
            return "VOC Management Plan"
        }
        if value ==  2124 {
            return "VOC Management Plan"
        }
        if value ==  2125 {
            return "VOC Management Plan"
        }
        if value ==  2126 {
            return "VOC Management Plan"
        }
        if value ==  2127 {
            return "VOC Management Plan"
        }
        if value ==  2128 {
            return "VOC Management Plan"
        }
        if value ==  2129 {
            return "VOC Management Plan"
        }
        if value ==  2130 {
            return "VOC Management Plan"
        }
        if value == 2131 {
            return "Cyber Security"
        }
        if value == 2132 {
            return "Cyber Security"
        }
        if value == 2133 {
            return "Cargo Lifting Equipment"
        }
        if value == 2134 {
            return "Cargo Lifting Equipment:"
        }
        if value ==  2135 {
            return "Life-saving equipment"
        }
        if value ==  2136 {
            return "Life-saving equipment"
        }
        if value ==  2137 {
            return "Life-saving equipment"
        }
        if value == 2138 {
            return "Ship to Ship Transfer Operations - Petroleum"
        }
        if value == 2139 {
            return "Ship to Ship Transfer Operations - Petroleum"
        }
        if value == 2140 {
            return "Ship to Ship Transfer Operations - Petroleum"
        }
        if value == 2141 {
            return "Extra Observations"
        }
        if value == 2142 {
            return "Extra Observations"
        }
        if value == 2143 {
            return "Extra Observations"
        }
        if value == 2144 {
            return "Extra Observations"
        }
        if value == 2145 {
            return "General appearance and condition"
        }
        if value == 1887 {
            return "General appearance and condition"
        }
        if value == 1893 {
            return "Pollution prevention"
        }
        if value == 1894 {
            return "Pollution prevention"
        }
        if value == 1896 {
            return "Pollution prevention"
        }
        if value == 1897 {
            return "Pump Rooms and Oil Discharge Monitors"
        }
        if value == 1898 {
            return "Pump Rooms and Oil Discharge Monitors"
        }
        if value == 1899 {
            return "Engine and steering compartments"
        }
        if value == 1900 {
            return "Engine and steering compartments"
        }
        if value == 1901 {
            return "Maritime Security"
        }
        if value == 1902 {
            return "Policies and Procedures"
        }
        if value == 1903 {
            return "Cargo and Ballast Systems - Petroleum"
        }
        if value == 1904 {
            return "Policies, Procedures and Documentation"
        }
        if value == 1905 {
            return "Stability and Cargo Loading Limitations"
        }
        if value ==  1906 {
            return "Cargo Operations and Related Safety Management"
        }
        if value ==  1907 {
            return "Cargo Operations and Related Safety Management"
        }
        if value ==  1908 {
            return "Cargo Operations and Related Safety Management"
        }
        if value == 1909 {
            return "Venting Arrangements"
        }
        if value == 1910 {
            return "Inert Gas System"
        }
        if value == 1911 {
            return "Crude Oil Washing"
        }
         if value == 1912 {
            return "Static Electricity Precautions"
        }
        if value == 1913 {
            return "Manifold Arrangements"
        }
        if value == 1914 {
            return "Pump Rooms"
        }
        if value == 1915 {
            return "Cargo Hoses"
        }
        if value == 1916 {
            return "Cargo Hoses"
        }
        if value == 1917 {
            return "Cargo Hoses"
        }
        if value == 1918 {
            return "Cargo Hoses"
        }
        if value == 1919 {
            return "Cargo Hoses"
        }
        if value == 1920 {
            return "Cargo Hoses"
        }
        if value == 1921 {
            return "Cargo Hoses"
        }
        if value == 1922 {
            return "Cargo Hoses"
        }
        if value == 1923 {
            return "Cargo Hoses"
        }
        if value == 1924 {
            return "Cargo Hoses"
        }
        if value == 1925 {
            return "Cargo Hoses"
        }
        if value == 1926 {
            return "Cargo and Ballast Systems Chemical"
        }
        if value ==  1927 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1928 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1929 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1930 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1931 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1932 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1933 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1934 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1935 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1936 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1937 {
            return "Policies Procedures & Documentation"
        }
        if value ==  1938 {
            return "Policies Procedures & Documentation"
        }
        if value == 1939 {
            return "Cargo and ballast systems  gas"
        }
        if value == 1940 {
            return "Policies Procedures & Documentation"
        }
        if value == 1941 {
           return "Stability and Cargo Loading Limitations"
        }
        if value == 1942 {
            return "Cargo Operations and Related Safety Management"
        }
        if value == 1943 {
            return "Cargo Handling and Monitoring Equipment"
        }
        if value == 1944 {
            return "Cargo Compressor and Motor Rooms"
        }
        if value == 1945 {
            return "Void Spaces and Seals - Type C Cargo Tanks"
        }
        if value == 1946 {
            return "Void and Interbarrier Spaces and Seals - Other Cargo Tank Types"
        }
        if value == 1947 {
            return "Inert Gas System "
        }
        if value == 1948 {
            return "Pressure Relief and Venting Systems"
        }
        if value == 1949 {
            return "Emergency Shut Down System"
        }
        if value == 1950 {
           return "Manifold Arrangements"
        }
        if value == 1951 {
           return "Safety Equipment"
        }
        if value == 1952 {
            return "Cargo Hoses"
        }
        if value == 1953 {
         return "Cargo Lifting Equipment"
        }
        if value == 1954 {
            return "LNG Carriers"
        }
        if value == 1955 {
            return "Ship to Ship Transfer Operations - Gas"
        }
        if value == 1957 {
            return "Ship to Ship Transfer Operations - Gas"
        }
        if value == 1958 {
            return "Mooring Procedures"
        }
        if value == 1956 {
            return "Mooring Procedures"
        }
        if value == 1959 {
            return "Mooring Equipment"
        }
        if value == 1960 {
            return "Anchoring Equipment"
        }
        if value ==  1961 {
            return "Single Point Moorings"
        }
        if value == 1962 {
            return "Emergency Towing Arrangements"
        }
        if value == 1963 {
            return "Emergency Towing Arrangements"
        }
        if value == 1964 {
            return "Emergency Towing Arrangements"
        }
        if value == 2147 {
            return "Emergency Towing Arrangements"
        }
        if value == 2148 {
            return "Emergency Towing Arrangements"
        }
        if value == 2149 {
            return "Emergency Towing Arrangements"
        }
        if value == 2150 {
            return "Emergency Towing Arrangements"
        }
        if value == 2091 {
            return "Emergency Towing Arrangements"
        }
        if value == 2121 {
            return "Emergency Towing Arrangements"
        }
        if value == 2146 {
            return "General appearance and condition"
        }
        if value == 2048 {
            return "Certification"
        }
        if value == 2049 {
            return "Certification"
        }
        if value == 2151 {
            return "Certification"
        }
        if value == 2152 {
            return "Certification"
        }
        if value == 2153 {
            return "Certification"
        }
        if value == 2154 {
            return "Certification"
        }
        if value == 2155 {
            return "Certification"
        }
        if value == 2156 {
            return "Certification"
        }
        if value == 2157 {
            return "Certification"
        }
        if value == 2158 {
            return "Certification"
        }
        if value == 2159 {
            return "Certification"
        }
        if value == 2160 {
            return "Certification"
        }
        if value == 2161 {
            return "Certification"
        }
        if value == 2162 {
            return "Certification"
        }
        if value == 2163 {
            return "Part B: Dynamic Assessment"
        }
        if value == 2164 {
            return "Part B: Dynamic Assessment"
        }
        if value == 2165 {
            return "Part A: Static Assessment"
        }
        if value == 2166 {
            return "Navigational Assessment and Audit"
        }
        if value == 2167 {
            return "Navigational Assessment and Audit"
        }
        if value == 2169 {
            return "Navigational Assessment and Audit"
        }
        if value == 2170 {
            return "Navigational Assessment and Audit"
        }
        if value == 2171 {
            return "Navigational Assessment and Audit"
        }
        if value == 2180 {
            return "Company Policy"
        }
        if value == 2182 {
            return "Passage Planning"
        }
        if value == 2184 {
            return "Bridge Equipment"
        }
        if value == 2185 {
            return "Forms and Checklists"
        }
        if value == 2186 {
            return "Company Policy"
        }
        if value == 2099 {
            return "Inert Gas Systems"
        }
        if value == 2007 {
            return "Inert Gas Systems"
        }
        if value == 2008 {
            return "Material Safety Datasheets (MSDS)"
        }
        if value == 2009 {
            return "Material Safety Datasheets (MSDS)"
        }
        if value == 2010 {
            return "Material Safety Datasheets (MSDS)"
        }
        if value == 2011 {
            return "Navigation equipment {All vessels constructed before 1st July 2002}"
        }
        if value == 2012 {
            return "All ships irrespective of size"
        }
        if value == 2013 {
            return "All ships of 150gt and upwards"
        }
        if value == 2014 {
            return "All ships of 300gt and upwards on international voyages"
        }
        if value == 2015 {
            return "All ships of 500gt and upwards"
        }
         if value == 2016 {
            return "All ships of 10000gt and upwards"
        }
        if value == 2017 {
            return "All ships of 100000gt and upwards"
        }
        if value == 2018 {
            return "Navigation equipment (All vessels constructed (i.e. keel laid) after  1st July 2002)"
        }
        if value == 2019 {
            return "All ships irrespective of size"
        }
        if value == 2020 {
            return "All ships of 150gt and upwards"
        }
        if value == 2021 {
            return "All ships of 300gt and upwards"
        }
        if value == 2022 {
            return "All ships of 300gt and upwards on international voyages"
        }
        if value == 2023 {
            return "All ships of 500gt and upwards"
        }
        if value ==  2024 {
            return "All ships of 3000gt and upwards"
        }
        if value == 2006 {
            return "All ships of 3000gt and upwards"
        }
         if value == 2025 {
            return "All ships of 10000gt and upwards"
        }
        if value == 2026 {
            return "All ships of 50000gt and upwards"
        }
        if value == 2100 {
            return "Venting Arrangements"
        }
            if value == 2028 {
            return "Venting Arrangements"
        }
       if value == 2101 {
            return "Static Electricity Precautions"
        }
        if value == 2102 {
            return "Manifold Arrangements"
        }
        if value == 2103 {
            return "Cargo Pump Room"
        }
        if value == 2104 {
            return "Safety Equipment"
        }
        if value == 2105 {
            return "Cargo Hoses"
        }
        if value == 2106 {
            return "Cargo Lifting Equipment"
        }
        if value == 2107 {
            return "Cargo Lifting Equipment"
        }
        if value == 2108 {
            return "Engine and steering compartments"
        }
        if value == 2109 {
            return "Fire Fighting Equipment"
        }
        if value == 2110 {
            return "Fire Fighting Equipment"
        }
        if value ==  2111 {
            return "Hull, superstructure and external weather decks "
        }
         if value == 2112 {
            return "Electrical equipment"
        }
        if value == 2113 {
            return "Internal spaces"
        }
        if value == 2114 {
            return "Internal spaces"
        }
        if value == 2115 {
            return "Policies, procedures and documentation"
        }
        if value == 2116 {
            return "Mooring"
        }
        if value == 2117 {
            return "Mooring"
        }
        if value ==  2118 {
            return "Navigation and Communications"
        }
        if value == 2119 {
            return "Drills, Training and Familiarisation"
        }
        if value == 2122 {
            return "Monitoring non-cargo spaces"
        }
        if value == 2187 {
            return "Bridge team organisation"
        }
        if value == 2188 {
            return "Duties"
        }
        if value == 2189 {
            return "General Navigation"
        }
        if value == 2190 {
            return "Passage Planning"
        }
        if value == 2191 {
            return "Use and Understanding of Bridge Equipment"
        }
        if value == 2192 {
            return "Pilotage"
        }
        if value == 1895 {
            return "Pilotage"
        }
        if value == 1965 {
            return "Pilotage"
        }
        if value == 1966 {
            return "Pilotage"
        }
        if value == 1967 {
            return "Pilotage"
        }
        if value == 1968 {
            return "Policies, procedures and documentation"
        }
        if value == 1969 {
            return "Planned maintenance"
        }
        if value == 1970 {
            return "Safety Management"
        }
        if value == 1971 {
            return "Machinery status"
        }
        if value == 1972 {
            return "Steering Compartment"
        }
        if value == 1973 {
            return "Steering Compartment"
        }
        if value == 1974 {
            return "Steering Compartment"
        }
        if value == 1975 {
            return "Steering Compartment"
        }
        if value == 1976 {
            return "Steering Compartment"
        }
        if value == 1977 {
            return "Accommodation areas"
        }
        if value == 1978 {
            return "General Information"
        }
        if value == 2089 {
           return "Gas analysing equipment"
        }
        if value == 2172 {
           return "Gas analysing equipment"
        }
        if value == 2173 {
           return "Gas analysing equipment"
        }
        if value == 2174 {
           return "Gas analysing equipment"
        }
        if value == 2175 {
           return "Gas analysing equipment"
        }
        if value == 2176 {
           return "Gas analysing equipment"
        }
        if value == 2177 {
           return "Gas analysing equipment"
        }
        if value == 2178 {
           return "Gas analysing equipment"
        }
        
        return "Basic"
    }
    
}
