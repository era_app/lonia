//
//  TestVC.swift
//  Lonia
//
//  Created by SSR Lab on 22.03.22.
//

import UIKit
import Alamofire
import SwiftyJSON


class TestVC: UIViewController {
    
    let touchedView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.layer.cornerRadius = 25
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(touchedView)
        tapView()
    
       sendAnswers()
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: self.view)
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else  { return }
                sSelf.touchedView.frame = CGRect(x: position.x - 25, y: position.y - 25, width: 50, height: 50)
                sSelf.view.layoutIfNeeded()
            }
            
        }
    }
    
    
    
    
    func getArray() -> [String: Any] {
        var answers: [String: Any] = [
            "answer_1": "name"
        ]
        
        var index = 0
        for _ in 0...10 {
            index += 1
//            let address = ["answer": [
//                "name": "Ilya",
//                "surname": "Rabyko"
//            ]]
//
            answers["answer"] = answers["answer_\(index)"]
        }
        
        var answerIndex = 0
        for _ in 0...10 {
            answerIndex += 1
            answers["answer_\(answerIndex)"] = [
                "answer":1,
                "comment":"i dont now",
                "questionid":"212",
                "question":"Work?",
                "questioncode":"12e2",
                "categoryid":23132,
                "categorynewid":"321321",
                "origin":"3213213"
                
            ]
        }
        
        let array: [String: Any] = [
            "briefcase": [
                "name_case":"s",
                "InspectorName":"Pavel",
                "InspectionTypes":"tes",
                "InspectionSource":"postman",
                "vessel":"office",
                "port":"Minsk",
                "date_in_vessel": "2022-03-23"
            ],
            "answer": answers
            
        ]
        

        return array
    }
    
    
    func sendAnswers() {
//        let params = getArray()
        
        let array = getArray()
            
        
        
        let headers: HTTPHeaders = [
            .authorization(bearerToken: Settings.shared.userToken),
            .contentType("application/json"),
            .accept("application/json")
                
            //"Content-Type": "application/json",
            //"Content-Type": "application/x-www-form-urlencoded",
            // "Accept": "application/json",
            // "Accept": "multipart/form-data"
        ]
        AF.request("http://dev.eraappmobile.com/api/answer", method: .post, parameters: array, encoding: JSONEncoding.default, headers: headers)
            .responseString(encoding: .utf8) { result in
                switch result.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    
                case .failure(let error):
                    print(error)
                }
            }
    }
    
    func tapView() {
        let tap = UIGestureRecognizer(target: self, action: #selector(tap))
        self.touchedView.addGestureRecognizer(tap)
    }
    
    @objc func tap() {
        UIView.animate(withDuration: 0.3) {  [weak self] in
            guard let sSelf = self else  { return }
            sSelf.touchedView.alpha = 0
            
        }
    }
    
    
    
}
