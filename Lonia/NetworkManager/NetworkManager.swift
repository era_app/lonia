//
//  NetworkManager.swift
//  Lonia
//
//  Created by SSR Lab on 16.03.22.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

final class NetworkManager {
    static let shared = NetworkManager()
}

extension NetworkManager {
    
    
    
    func sendAnswersAndBriefcase(briefcase: SavedBrifcase, completion: @escaping (Result<String, Error>) -> Void) {
        var answers: [String: Any] = [
            "answer_1": "name"
        ]
        
        var answerIndexFirst = 0
        for _ in briefcase.questions {
            answerIndexFirst += 1
            answers["answer"] =  answers["answer_\(answerIndexFirst)"]
        }
        
        var answerIndex = 0
        for question in briefcase.questions {
            answerIndex += 1
            var images: [String: Any] = [
                "foto_1": "name"
            ]
            
            
            if question.images.count != 0 {
                var photoIndex = 0
                for image in question.images {
                    photoIndex += 1
                    images["foto_\(photoIndex)"] = image
                    
                }
                answers["answer_\(answerIndex)"] = [
                    "answer": Int(question.answer) ?? 0 ,
                    "comment": question.comment,
                    "questionid": question.questionID,
                    "question": question.question,
                    "questioncode": question.questionCode,
                    "categoryid": Int(question.categoryID) ?? 0,
                    "categorynewid":  question.categoryNewID,
                    "origin": question.origin,
                    "data_image": images
                ]
                
            } else {
                answers["answer_\(answerIndex)"] = [
                    "answer": Int(question.answer) ?? 0 ,
                    "comment": question.comment,
                    "questionid": question.questionID,
                    "question": question.question,
                    "questioncode": question.questionCode,
                    "categoryid": Int(question.categoryID) ?? 0,
                    "categorynewid":  question.categoryNewID,
                    "origin": question.origin
                ]
            }
        }
        
        let params: [String: Any] = [
            "briefcase": [
                "name_case": "\(briefcase.inspectorName)_\(briefcase.questionaries)_\(briefcase.dateOfCreation)",
                "InspectorName": briefcase.inspectorName,
                "InspectionTypes": briefcase.inspectorType,
                "InspectionSource": briefcase.inspectorSource,
                "vessel": briefcase.vessel,
                "port": briefcase.port,
                "date_in_vessel": "\(briefcase.dateOfCreation)"
            ],
            "answer": answers
            
        ]
        
 
        let headers: HTTPHeaders = [
            .authorization(bearerToken: Settings.shared.userToken),
            .contentType("application/json"),
            .accept("application/json")
        ]
        
        AF.request("https://dev.eraapp.ru/api/answer", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{ result in
                switch result.result {
                case .success(let value):
                    let json = JSON(value)
                    guard let status = json["status"].string else { return }
                    status == "Success!" ? completion(.success(status)) : completion(.failure(DatabaseError.FailedToGetData))
                case .failure(let error):
                    completion(.failure(DatabaseError.FailedToGetData))
                    print(error)
                }
            }
        
    }
    
    
    
    func getToken(login: String, password: String, completion: @escaping (Result<String, Error>) -> Void) {
        
        let params: [String: Any] = [
            "email": login,
            "password": password
        ]
        
        AF.request("https://dev.eraapp.ru/login", method: .post, parameters: params).responseJSON { result in
            switch result.result {
            case .success(let value):
                let json = JSON(value)
                guard let token = json["token"].string else {
                    completion(.failure(DatabaseError.FailedToGetData))
                    return }
                Settings.shared.userToken = token
                completion(.success(token))
            case .failure(let error):
                print(error)
                completion(.failure(DatabaseError.FailedToGetData))
            }
        }
    }
    
    func getCategory(completion: @escaping (Result<[CategoryID], Error>) -> Void) {
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Settings.shared.userToken)]
        AF.request("https://dev.eraapp.ru/api/question", method: .get, headers: headers).responseJSON { result in
            switch result.result {
            case .success(let value):
                let json = JSON(value)
                var categoriesArray: [CategoryID] = []
                if json.arrayValue.count == 0 {
                    completion(.failure(DatabaseError.FailedToGetData))
                } else {
                for item in json.arrayValue {
                    guard let qid = item["qid"].int else { return }
                    guard let title = item["title"].string else { return }
                    let category = CategoryID(id: qid, title: title)
                    categoriesArray.append(category)
                }
                    completion(.success(categoriesArray))
                }
                
            case .failure(let error):
                print(error)
                completion(.failure(DatabaseError.FailedToGetData))
            }
        }
    }
    
    func getQuestions(id: Int, completion: @escaping (Result<[Questions], Error>) -> Void) {
        
        let params: [String: Any] = [
            "qid": id
        ]
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Settings.shared.userToken)]
        AF.request("https://dev.eraapp.ru/api/question", method: .post, parameters: params, headers: headers).responseJSON { result in
            switch result.result {
            case .success(let value):
                let json = JSON(value)
                var questions: [Questions] = []
                if json.arrayValue.count == 0 {
                    completion(.failure(DatabaseError.FailedToGetData))
                } else {
                for item in json.arrayValue {
                    guard let questionID    =  item["questionid"].string else {
                        completion(.failure(DatabaseError.FailedToGetData))
                        return }
                    guard let questionCode  =  item["questioncode"].string else {
                        completion(.failure(DatabaseError.FailedToGetData))
                        return }
                    guard let question      =  item["question"].string else {
                        completion(.failure(DatabaseError.FailedToGetData))
                        return }
                    guard let comment       =  item["comment"].string else {
                        completion(.failure(DatabaseError.FailedToGetData))
                        return }
                    guard let categoryID    =  item["categoryid"].string else {
                        completion(.failure(DatabaseError.FailedToGetData))
                        return }
                    guard let origin        =  item["origin"].string else {
                        completion(.failure(DatabaseError.FailedToGetData))
                        return }
                    guard let categoryNewID =  item["categorynewid"].string else {
                        completion(.failure(DatabaseError.FailedToGetData))
                        return }
                    
                    let currentQuestion = Questions(questionID: questionID, questionCode: questionCode, question: question, comment: comment, categoryID: categoryID, origin: origin, categoryNewID: categoryNewID)
                    questions.append(currentQuestion)
                }
                    completion(.success(questions))
                }
//                completion(questions)
            case .failure(let error):
                print(error)
                completion(.failure(DatabaseError.FailedToGetData))
            }
            
        }
    }
    
    
    
    func getBriefcaseInformation(completion: @escaping (Result<BriefcaseInfo, Error>) -> Void) {
        

        let headers: HTTPHeaders = [.authorization(bearerToken: Settings.shared.userToken)]
        AF.request("https://dev.eraapp.ru/api/infobiefcase", method: .get, headers: headers).responseJSON { result in
            switch result.result {
            case .success(let value):
                let json = JSON(value)
                var ports: [String] = []
                var inspectionSources: [String] = []
                var inspectionsType: [String] = []
                var vessels: [String] = []
                
                
                for portsJson in json["port"].arrayValue {
                    guard let detectedPort = portsJson.string else { return }
                    ports.append(detectedPort)
                }
                
                for inspectionSourcesJson in json["inspecstion_source"].arrayValue {
                    guard let detectedInspectionSources = inspectionSourcesJson.string else { return }
                    inspectionSources.append(detectedInspectionSources)
                }
                
                for inspectioninspectionsTypeJson in json["inspection_type"].arrayValue {
                    guard let detectedInspectionType = inspectioninspectionsTypeJson.string else { return }
                    inspectionsType.append(detectedInspectionType)
                }
                
                for vesselsJson in json["vessel"].arrayValue {
                    guard let detectedvessel = vesselsJson.string else { return }
                    vessels.append(detectedvessel)
                }
                
                completion(.success(BriefcaseInfo(ports: ports, inspectionTypes: inspectionsType, inspectionSource: inspectionSources, vessels: vessels)))
                
//                completion(questions)
            case .failure(let error):
                print(error)
                completion(.failure(DatabaseError.FailedToGetData))
            }
            
        }
    }
    
    
    
    
}

public enum DatabaseError: Error {
    case FailedToGetData
}


