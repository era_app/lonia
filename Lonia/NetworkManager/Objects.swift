//
//  Objects.swift
//  Lonia
//
//  Created by SSR Lab on 16.03.22.
//

import Foundation
import UIKit

struct CategoryID {
    var id: Int
    var title: String
}



struct Questions {
    var questionID: String
    var questionCode: String
    var question: String
    var comment: String
    var categoryID: String
    var origin: String
    var categoryNewID: String
}


struct BriefcaseInfo {
    let ports: [String]
    let inspectionTypes: [String]
    let inspectionSource: [String]
    let vessels: [String]
}





