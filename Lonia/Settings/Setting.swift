//
//  Setting.swift
//  Lonia
//
//  Created by SSR Lab on 16.03.22.
//

import Foundation
import UIKit

class Settings {
    static let shared = Settings()
    
    private init() {}
    private let defaults = UserDefaults.standard
    
    var isFirstOpen: Bool {
        set {
            defaults.set(newValue, forKey: "isFirstOpen")
        }
        get {
            return defaults.value(forKey: "isFirstOpen") as? Bool ?? true
        }
    }
    
    var userToken: String {
        set {
            defaults.set(newValue, forKey: "userToken")
        }
        get {
            return defaults.value(forKey: "userToken") as? String ?? ""
        }
    }

    
    
    
    
}
