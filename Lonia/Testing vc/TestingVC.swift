//
//  TestingVC.swift
//  Lonia
//
//  Created by Ilya Rabyko on 4.05.22.
//

import UIKit
import CoreImage
import ImageCaptureCore
import ImageIO
class TestingVC: UIViewController {

    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var testImageScreen: UIImageView!
    var image = UIImage(named: "testImage")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NetworkManager.shared.getBriefcaseInformation { result in
            switch result {
                
            case .success(let result):
                print(result)
            case .failure(_):
                print("Error")
            }
        }
        }
        
    
    
   
}
