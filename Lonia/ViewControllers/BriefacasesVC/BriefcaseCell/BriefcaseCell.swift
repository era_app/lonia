//
//  BriefcaseCell.swift
//  Lonia
//
//  Created by SSR Lab on 16.03.22.
//

import UIKit

class BriefcaseCell: UICollectionViewCell {
    
    @IBOutlet weak var vesselLabel: UILabel!
    
    @IBOutlet weak var portLabel: UILabel!
    
    @IBOutlet weak var inspectionTypeLabel: UILabel!
    
    @IBOutlet weak var inspectionSource: UILabel!
    
    @IBOutlet weak var questionnaireLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var totalQuestions: UILabel!
    
    @IBOutlet weak var answeredQuestions: UILabel!
    
    var isAnswers: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setupCell(briefCase: SavedBrifcase) {
        let dateformatter = DateFormatter() // 2-2
        dateformatter.dateStyle = .short
        dateformatter.dateFormat = "dd.MM.yyyy"
        
        vesselLabel.text = briefCase.vessel
        portLabel.text = briefCase.port
        inspectionTypeLabel.text = briefCase.inspectorType
        inspectionSource.text = briefCase.inspectorSource
        questionnaireLabel.text = briefCase.questionaries
        dateLabel.text = dateformatter.string(from: briefCase.dateOfCreation)
        
        if isAnswers {
            totalQuestions.text = "Total: \(briefCase.questions.count)"
            answeredQuestions.text = "Answered: \(briefCase.questions.filter({$0.isAnswered == true}).count)"
        } else {
            answeredQuestions.isHidden = true
            totalQuestions.isHidden = true
        }
        
    }
    
}
