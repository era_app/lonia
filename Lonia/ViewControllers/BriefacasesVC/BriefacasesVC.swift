//
//  BriefacasesVC.swift
//  Lonia
//
//  Created by SSR Lab on 16.03.22.
//

import UIKit


class BriefacasesVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var briefCases: [SavedBrifcase] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBriefcases()
    }
    
    
    func setupBriefcases() {
        self.setupNavigationBar(view: self)
        DispatchQueue.global().async { [weak self] in
            guard let sSelf = self else { return }
            if let briefcase = UserDefaults.standard.data(forKey: "questionUserDefaults") {
                do {
                    let decoder = JSONDecoder()
                    let briefcases = try decoder.decode([SavedBrifcase].self, from: briefcase)
                    if sSelf.briefCases.count > briefcases.count || sSelf.briefCases.count < briefcases.count  {
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            sSelf.briefCases = []
                            for currentBriefcase in briefcases {
                                if currentBriefcase.questions.count == currentBriefcase.questions.filter({$0.isAnswered == true}).count {
                                    print("Answered")
                                } else {
                                    print("Not answered")
                                    sSelf.briefCases.append(currentBriefcase)
                                }
                            }
                            sSelf.briefCases = sSelf.briefCases.sorted(by: {$0.dateOfCreation > $1.dateOfCreation})
                            sSelf.collectionView.reloadData()
                        }
                        }
                } catch {
                    print("Unable to Encode Note (\(error))")
                }
            }
        }
    
        
        
        
    }
    
    
    func setupController() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "BriefcaseCell", bundle: nil), forCellWithReuseIdentifier: "BriefcaseCell")
        
        self.title = "Briefcases"
        
        self.setupNavigationBar(view: self)
        
        setupBriefcases()
    }
    
    
    
    @IBAction func createBriefcaseAction(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            let vc = NewBriefcaseParametersVC(nibName: "NewBriefcaseParametersVC", bundle: nil)
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        } else {
            //            let alert = UIAlertController(title: "Error", message: "No internet connection", preferredStyle: .alert)
            //            alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
            //            self.present(alert, animated: true)
            let vc = NewBriefcaseParametersVC(nibName: "NewBriefcaseParametersVC", bundle: nil)
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
}

extension BriefacasesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return briefCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        NetworkManager.shared.sendAnswersAndBriefcase(briefcase: briefCases[indexPath.row]) { success in
//            print(success)
//        } failure: {
//            print("lol")
//        }
//
        let vc = QuestionsVC(nibName: "QuestionsVC", bundle: nil)
        vc.currentBriefcase = briefCases[indexPath.row]
        vc.contentType = .NotAnswered
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BriefcaseCell", for: indexPath) as! BriefcaseCell
        cell.setupCell(briefCase: briefCases[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: width, height: 350)
    }
    
    
}


extension UIImage {

    func resize(targetSize: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size:targetSize).image { _ in
            self.draw(in: CGRect(origin: .zero, size: targetSize))
        }
    }

}
