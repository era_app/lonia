//
//  LoginVC.swift
//  Lonia
//
//  Created by SSR Lab on 23.03.22.
//

import UIKit
import NVActivityIndicatorView


class LoginVC: UIViewController {
    
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var loginField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var sigInButton: UIButton!
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Settings.shared.userToken != "" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "MainTabBarController")
            mainTabBarController.modalPresentationStyle = .fullScreen
            self.present(mainTabBarController, animated: true, completion: nil)
        }
    }
    
    
    
    func setupController() {
        if Settings.shared.userToken != "" {
            loginView.isHidden = true
            passwordView.isHidden = true
            sigInButton.isHidden = true
        }
        
        indicator.isHidden = true
        indicator.color = UIColor.black
        indicator.type = .ballPulse
        
        self.overrideUserInterfaceStyle = .light
        
        self.hideKeyboardWhenTappedAround()
        
        loginField.delegate = self
        passwordField.delegate = self
        
        sigInButton.layer.cornerRadius = 15
        sigInButton.layer.borderColor = UIColor.black.cgColor
        sigInButton.layer.borderWidth = 1
        
        loginView.layer.cornerRadius = 15
        loginView.layer.borderColor = UIColor.black.cgColor
        loginView.layer.borderWidth = 1
        
        passwordView.layer.cornerRadius = 15
        passwordView.layer.borderColor = UIColor.black.cgColor
        passwordView.layer.borderWidth = 1
        
    }
    
    
    
    
    func alertController() {
        let alert = UIAlertController(title: "Error", message: "Check the correctness of the entered data", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        self.present(alert, animated: true)
    }
    
    @IBAction func sigInAction(_ sender: Any) {
        self.indicator.startAnimating()
        guard let login = loginField.text, !login.isEmpty else {
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            self.addBorder(view: loginView)
            return
        }
        guard let password = passwordField.text , !password.isEmpty else {
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            self.addBorder(view: passwordView)
            return }
        
        
        NetworkManager.shared.getToken(login: login.lowercased(), password: password.lowercased()) { result in
            switch result {
                
            case .success(let token):
                Settings.shared.userToken = token
                DispatchQueue.main.async { [weak self] in
                    guard let sSelf = self else { return }
                    sSelf.indicator.stopAnimating()
                    sSelf.indicator.isHidden = true
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let mainTabBarController = storyboard.instantiateViewController(identifier: "MainTabBarController")
                    mainTabBarController.modalPresentationStyle = .fullScreen
                    sSelf.present(mainTabBarController, animated: true, completion: nil)
                }
                
            case .failure(_):
                self.alertController()
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                print("Error")
            }
        }
//        NetworkManager.shared.getToken(login: login.lowercased(), password: password.lowercased()) { token in
//            Settings.shared.userToken = token
//            DispatchQueue.main.async { [weak self] in
//                guard let sSelf = self else { return }
//                sSelf.indicator.stopAnimating()
//                sSelf.indicator.isHidden = true
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let mainTabBarController = storyboard.instantiateViewController(identifier: "MainTabBarController")
//                mainTabBarController.modalPresentationStyle = .fullScreen
//                sSelf.present(mainTabBarController, animated: true, completion: nil)
//            }
//        } failure: {
//            DispatchQueue.main.async { [weak self] in
//                guard let sSelf = self else { return }
//                sSelf.alertController()
//                sSelf.indicator.isHidden = true
//                sSelf.indicator.stopAnimating()
//            }
//        }
        
    }
    
    
    
    
    
}


extension LoginVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.loginView.layer.borderColor = UIColor.black.cgColor
            sSelf.passwordView.layer.borderColor = UIColor.black.cgColor
        }
    }
}
