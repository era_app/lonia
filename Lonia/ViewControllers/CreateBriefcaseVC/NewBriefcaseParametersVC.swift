//
//  NewBriefcaseParametersVC.swift
//  Lonia
//
//  Created by SSR Lab on 16.03.22.
//

import UIKit
import NVActivityIndicatorView
import Lottie

enum BriefcaseContentType {
    case vessel
    case port
    case inspectionType
    case inspectorSource
    case questionnaires
    case inspectorName
    
}

class NewBriefcaseParametersVC: UIViewController {
    
    @IBOutlet weak var inspectorNameField: UITextField!
    
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var searchHeight: NSLayoutConstraint!
    
    @IBOutlet weak var search: UISearchBar!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var alphaView: UIView!
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    @IBOutlet weak var animationView: AnimationView!
    
    var vessels: [String] = []
    var inspectionType: [String] = []
    var dubInspectionSource: [String] = []
    var inspectionSource: [String] = []
    
    
    var dubPorts: [String] = []
    var ports: [String] = []
    
    var briefcasesInfo = BriefcaseInfo(ports: [], inspectionTypes: [], inspectionSource: [], vessels: [])
    
    var categoriesArray: [CategoryID] = []
    var briefCases: [SavedBrifcase] = []
    var contentType: BriefcaseContentType = .vessel
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    func setupController() {
        search.delegate = self
        
        alphaView.isHidden = true
        
        indicator.isHidden = true
        
        self.hideKeyboardWhenTappedAround()
        switch contentType {
        case .vessel:
            self.animationView.animation = Animation.named("vessel")
            animationView.loopMode = .loop
            animationView.contentMode = .scaleAspectFill
            animationView.play()
            
            indicator.isHidden = false
            indicator.color = UIColor.black
            indicator.type = .ballPulse
            alphaView.isHidden = false
            self.getBriefcasesInfo()
            self.setupArrays()
            searchHeight.constant = 0
            search.isHidden = true
            self.title = "Vessels"
        case .port:
            self.setupArrays()
            searchHeight.constant = 50
            search.isHidden = false
            self.ports = ports.sorted(by: {$0 > $1})
            self.title = "Port"
        case .inspectionType:
            self.setupArrays()
            searchHeight.constant = 0
            search.isHidden = true
            self.title = "Inspection Type"
        case .inspectorSource:
            self.setupArrays()
            searchHeight.constant = 59
            search.isHidden = false
            self.inspectionSource = inspectionSource.sorted(by: {$0 > $1})
            self.title = "Inspector Source"
        case .questionnaires:
            self.setupArrays()
            self.animationView.animation = Animation.named("vessel")
            animationView.loopMode = .loop
            animationView.contentMode = .scaleAspectFill
            animationView.play()
            
            indicator.isHidden = false
            indicator.color = UIColor.black
            indicator.type = .ballPulse
            alphaView.isHidden = false
            self.getCategories()
            
            if Settings.shared.isFirstOpen == true {
                let array = [SavedBrifcase]()
                do {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(array)
                    UserDefaults.standard.set(data, forKey: "questionUserDefaults")
                } catch {
                    print("Unable to Encode Note (\(error))")
                }
                
            } else {
                setupBriefcases()
            }
            
            searchHeight.constant = 0
            search.isHidden = true
            self.title = "Questionnaires"
        case .inspectorName:
            searchHeight.constant = 0
            search.isHidden = true
            continueButton.layer.borderColor = UIColor.black.cgColor
            continueButton.layer.borderWidth = 1
            collectionView.isHidden = true
            self.title = "Inspector Name"
        }
        collectionView.register(UINib(nibName: "CreateBriefcaseCell", bundle: nil), forCellWithReuseIdentifier: "CreateBriefcaseCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func getCategories() {
        NetworkManager.shared.getCategory { [weak self] result in
            guard let sSelf = self else { return }
            switch result {
            case .success(let categories):
                sSelf.categoriesArray = categories
                DispatchQueue.main.async { [weak self] in
                    guard let sSelf = self else { return }
                    sSelf.setupNextController()
                    sSelf.collectionView.reloadData()
                    sSelf.animationView.stop()
                }
            case .failure(_):
                DispatchQueue.main.async { [weak self] in
                    guard let sSelf = self else { return }
                    let alertControler = UIAlertController(title: "Error", message: "Try again later", preferredStyle: .alert)
                    alertControler.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            sSelf.navigationController?.popToRootViewController(animated: true)
                        }
                    }))
                    sSelf.present(alertControler, animated: true)
                    sSelf.setupNextController()
                }
                
            }
        }
    }
    
    
    func setupBriefcases() {
        if let briefcase = UserDefaults.standard.data(forKey: "questionUserDefaults") {
            do {
                let decoder = JSONDecoder()
                let briefcases = try decoder.decode([SavedBrifcase].self, from: briefcase)
                self.briefCases = briefcases
            } catch {
                print("Unable to Encode Note (\(error))")
            }
        }
    }
    
    func setupArrays() {
        self.vessels = briefcasesInfo.vessels
        self.ports = briefcasesInfo.ports
        self.dubPorts = briefcasesInfo.ports
        self.inspectionType = briefcasesInfo.inspectionTypes
        self.inspectionSource = briefcasesInfo.inspectionSource
        self.dubInspectionSource = briefcasesInfo.inspectionSource
    }
    
    func setupDownloading() {
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = true
        indicator.isHidden = false
        alphaView.isHidden = false
        indicator.color = UIColor.black
        indicator.type = .ballPulse
        indicator.startAnimating()
        animationView.play()
        
    }
    
    func setupNextController() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        indicator.isHidden = true
        alphaView.isHidden = true
        indicator.stopAnimating()
        animationView.stop()
        
    }
    
    func getBriefcasesInfo() {
        self.setupDownloading()
        NetworkManager.shared.getBriefcaseInformation { result in
            switch result {
            case .success(let information):
                self.briefcasesInfo = information
                self.vessels = information.vessels
                self.ports = information.ports
                self.dubPorts = information.ports
                self.inspectionType = information.inspectionTypes
                self.inspectionSource = information.inspectionSource
                self.dubInspectionSource = information.inspectionSource
                DispatchQueue.main.async { [weak self] in
                    guard let sSelf = self else { return }
                    sSelf.setupNextController()
                    sSelf.collectionView.reloadData()
                }
            case .failure(_):
                let alert = UIAlertController(title: "Error", message: "Check your network connection", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Close", style: .cancel))
            }
        }
    }
    
    func openAlert(choice: String, id: Int) {
        var alertController = UIAlertController()
        switch contentType {
        case .vessel:
            alertController = UIAlertController(title: "Vessel", message: "Are you sure that your vessel is \(choice)?", preferredStyle: .actionSheet)
        case .port:
            alertController = UIAlertController(title: "Port", message: "Are you sure that your port is \(choice)?", preferredStyle: .actionSheet)
            self.title = "Port"
        case .inspectionType:
            alertController = UIAlertController(title: "Inspection Type", message: "Are you sure that your inspection type is \(choice)?", preferredStyle: .actionSheet)
            self.title = "Inspection Type"
        case .inspectorSource:
            alertController = UIAlertController(title: "Inspector Source", message: "Are you sure that your inspector source is \(choice)?", preferredStyle: .actionSheet)
            self.title = "Inspector"
        case .questionnaires:
            alertController = UIAlertController(title: "Questionnaire", message: "Are you sure that your Questionnaire is \(choice)?", preferredStyle: .actionSheet)
            self.title = "Questionnaires"
        case .inspectorName:
            self.title = "inspector Name"
            
        }
        alertController.addAction(UIAlertAction(title: "Continue", style: .default, handler: {[ weak self ]_ in
            guard let sSelf = self else { return }
            switch sSelf.contentType {
            case .vessel:
                UserDefaults.standard.set(choice, forKey: "vessel")
                let vc = NewBriefcaseParametersVC(nibName: "NewBriefcaseParametersVC", bundle: nil)
                vc.briefcasesInfo = self!.briefcasesInfo
                vc.contentType = .port
                sSelf.navigationController?.pushViewController(vc, animated: true)
            case .port:
                UserDefaults.standard.set(choice, forKey: "port")
                let vc = NewBriefcaseParametersVC(nibName: "NewBriefcaseParametersVC", bundle: nil)
                vc.briefcasesInfo = self!.briefcasesInfo
                vc.contentType = .inspectionType
                sSelf.navigationController?.pushViewController(vc, animated: true)
            case .inspectionType:
                UserDefaults.standard.set(choice, forKey: "inspectionType")
                let vc = NewBriefcaseParametersVC(nibName: "NewBriefcaseParametersVC", bundle: nil)
                vc.briefcasesInfo = self!.briefcasesInfo
                vc.contentType = .inspectorSource
                sSelf.navigationController?.pushViewController(vc, animated: true)
            case .inspectorSource:
                UserDefaults.standard.set(choice, forKey: "inspectorSource")
                let vc = NewBriefcaseParametersVC(nibName: "NewBriefcaseParametersVC", bundle: nil)
                vc.briefcasesInfo = self!.briefcasesInfo
                vc.contentType = .inspectorName
                sSelf.navigationController?.pushViewController(vc, animated: true)
            case .questionnaires:
                UserDefaults.standard.set(choice, forKey: "Category")
                
                guard let vessel = UserDefaults.standard.value(forKey: "vessel") as? String else { return }
                guard let port = UserDefaults.standard.value(forKey: "port") as? String else { return }
                guard let inspectionType = UserDefaults.standard.value(forKey: "inspectionType") as? String else { return }
                guard let inspectorSource = UserDefaults.standard.value(forKey: "inspectorSource") as? String else { return }
                guard let inspectorName = UserDefaults.standard.value(forKey: "inspectorName") as? String else { return }
                
                sSelf.setupDownloading()
                
                NetworkManager.shared.getQuestions(id: id) { [weak self] result in
                    switch result {
                    case .success(let questions):
                        var categoriesName: [String] = []
                        
                        for categoryID in questions {
                            if let questionCategoryID = Int(categoryID.categoryID) {
                                categoriesName.append(sSelf.getCategoryName(value: questionCategoryID))
                                print(sSelf.getCategoryName(value: questionCategoryID))
                            } else {
                                categoriesName.append("Basic")
                            }
                        }
                        
                        let newBriefcase = SavedBrifcase(inspector: inspectorSource, inspectorName: inspectorName, inspectorType: inspectionType, vessel: vessel, category: choice, question: [], isAnswered: false, port: port, dateOfCreation: Date(), notes: [], categories: categoriesName.uniqued(), isSended: false)
            
                        
                        for question in questions {
                            
                            if let questionCategoryID = Int(question.categoryID) {
                                newBriefcase.questions.append(QuestionsSave(questionCode: question.questionCode, question: question.question, comment: "", categoryID: question.categoryID, origin: question.origin, categoryNewID: question.categoryNewID, isAnswered: false, answer: "0", dateOfInspection: "", images: [], questionID: question.questionID, categoryName: sSelf.getCategoryName(value: questionCategoryID), questionInformation: question.comment, significant: ""))
                            } else {
                                newBriefcase.questions.append(QuestionsSave(questionCode: question.questionCode, question: question.question, comment: "", categoryID: question.categoryID, origin: question.origin, categoryNewID: question.categoryNewID, isAnswered: false, answer: "0", dateOfInspection: "", images: [], questionID: question.questionID, categoryName: "Basic", questionInformation: question.comment, significant: ""))
                            }
                            print("id:\(question.categoryID)")
                        }
                        
                        sSelf.briefCases.append(newBriefcase)
                        
                        do {
                            let encoder = JSONEncoder()
                            let data = try encoder.encode(sSelf.briefCases)
                            UserDefaults.standard.set(data, forKey: "questionUserDefaults")
                        } catch {
                            print("Unable to Encode Note (\(error))")
                        }
                        
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            sSelf.tabBarController?.tabBar.isHidden = false
                            Settings.shared.isFirstOpen = false
                            sSelf.navigationController?.popToRootViewController(animated: true)
                        }
                    case .failure(_):
                        let alertControler = UIAlertController(title: "Error", message: "Try again later", preferredStyle: .alert)
                        alertControler.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                            DispatchQueue.main.async { [weak self] in
                                guard let sSelf = self else { return }
                                sSelf.setupNextController()
                                sSelf.navigationController?.popViewController(animated: true)
                            }
                        }))
                        
                        self?.present(alertControler, animated: true)
                    }
                }
                
            
            case .inspectorName:
                UserDefaults.standard.set(choice, forKey: "inspectorName")
                let vc = NewBriefcaseParametersVC(nibName: "NewBriefcaseParametersVC", bundle: nil)
                vc.contentType = .questionnaires
                sSelf.setupDownloading()
                vc.hidesBottomBarWhenPushed = true
                sSelf.navigationController?.pushViewController(vc, animated: true)
            }
            
        }))
        
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {[ weak self ]_ in
            guard let sSelf = self else { return }
            print(sSelf)
            print("Cancel")
        }))
        
        DispatchQueue.main.async { [weak self] in
            guard let sSelf = self else { return }
            if let popoverController = alertController.popoverPresentationController {
                print(popoverController)
                sSelf.addActionSheetForiPad(actionSheet: alertController)
                sSelf.present(alertController, animated: true, completion: nil)
            } else {
            sSelf.present(alertController, animated: true)
            }
        }
        
    }
    
    
    @IBAction func continueAction(_ sender: Any) {
        guard let inspectorName = inspectorNameField.text, !inspectorName.isEmpty else { return }
        UserDefaults.standard.set(inspectorName, forKey: "inspectorName")
        let vc = NewBriefcaseParametersVC(nibName: "NewBriefcaseParametersVC", bundle: nil)
        vc.contentType = .questionnaires
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}


extension NewBriefcaseParametersVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch contentType {
        case .vessel:
            openAlert(choice: vessels[indexPath.row], id: indexPath.row)
        case .port:
            openAlert(choice: dubPorts[indexPath.row], id: indexPath.row)
        case .inspectionType:
            openAlert(choice: inspectionType[indexPath.row], id: indexPath.row)
        case .inspectorSource:
            openAlert(choice: dubInspectionSource[indexPath.row], id: indexPath.row)
        case .questionnaires:
            openAlert(choice: categoriesArray[indexPath.row].title, id: categoriesArray[indexPath.row].id)
        case .inspectorName:
            print("inspectorName")
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch contentType {
        case .vessel:
            return vessels.count
        case .port:
            return dubPorts.count
        case .inspectionType:
            return inspectionType.count
        case .inspectorSource:
            return dubInspectionSource.count
        case .questionnaires:
            return categoriesArray.count
        case .inspectorName:
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreateBriefcaseCell", for: indexPath) as! CreateBriefcaseCell
        
        switch self.contentType {
        case .vessel:
            cell.mainLabel.text = vessels[indexPath.row]
            cell.mainImage.image = UIImage(named: "vessel")
        case .port:
            cell.mainLabel.text = dubPorts[indexPath.row]
            cell.mainImage.image = UIImage(named: "port")
        case .inspectionType:
            cell.mainLabel.text = inspectionType[indexPath.row]
            cell.mainImage.image = UIImage(named: "inspection")
        case .inspectorSource:
            cell.mainLabel.text = dubInspectionSource[indexPath.row]
            cell.mainImage.image = UIImage(named: "inspection")
        case .questionnaires:
            cell.mainLabel.text = categoriesArray[indexPath.row].title
            cell.mainImage.image = UIImage(named: "inspection")
        case .inspectorName:
            print("inspectorName")
        }
        
        cell.contentType = contentType
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewSize = collectionView.frame.size.width
        return CGSize(width: collectionViewSize, height: 85)
    }
}

extension NewBriefcaseParametersVC: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        search.setShowsCancelButton(false, animated: true)
        search.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if self.contentType == .port {
            dubPorts = []
            if searchText == "" {
                dubPorts = ports.sorted(by: {$0 > $1})
            }
            
            for item in ports {
                if item.uppercased().contains(searchText.uppercased()) {
                    dubPorts.append(item)
                }
            }
            self.dubPorts = dubPorts.sorted(by: {$0 > $1})
            collectionView.reloadData()
        }
        
        if self.contentType == .inspectorSource {
            dubInspectionSource = []
            if searchText == "" {
                dubInspectionSource = inspectionSource.sorted(by: {$0 > $1})
            }
            
            for item in inspectionSource {
                if item.uppercased().contains(searchText.uppercased()) {
                    dubInspectionSource.append(item)
                }
            }
            collectionView.reloadData()
        }
    }
}


extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }

    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}
