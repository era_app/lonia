//
//  CategoriesVC.swift
//  Lonia
//
//  Created by SSR Lab on 16.03.22.
//

import UIKit

class CategoriesVC: UIViewController {

    
    var categories: [CategoryID] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    func setupController() {
        downloadCategories()
    }
    
    func downloadCategories() {
        NetworkManager.shared.getCategory { [weak self] categories in
            guard let sSelf = self else { return }
            sSelf.categories = categories
        } failure: {
            print("Error")
        }

    }

}
