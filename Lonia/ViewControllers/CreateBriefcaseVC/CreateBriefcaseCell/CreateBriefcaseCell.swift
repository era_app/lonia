//
//  CreateBriefcaseCell.swift
//  Lonia
//
//  Created by SSR Lab on 16.03.22.
//

import UIKit

class CreateBriefcaseCell: UICollectionViewCell {
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var mainLabel: UILabel!
    
    var contentType: BriefcaseContentType = .vessel
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell() {
        switch contentType {
        case .vessel:
            mainImage.image = UIImage(named: "vessel")
        case .port:
            mainImage.image = UIImage(named: "port")
        case .inspectionType:
            mainImage.image = UIImage(named: "inspection")
        case .inspectorSource:
            mainImage.image = UIImage(named: "inspection")
        case .questionnaires:
            print("")
        case .inspectorName:
            print("")
        }
    }
    
}
