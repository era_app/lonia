//
//  AnswerForQuestionArrayVC.swift
//  Lonia
//
//  Created by SSR Lab on 5.04.22.
//

import UIKit

class AnswerForQuestionArrayVC: UIViewController {

    @IBOutlet weak var dateField: UITextField!
    
    @IBOutlet weak var commentTextView: UITextView!
    
    
    @IBOutlet weak var commentView: UIView!
    
    @IBOutlet weak var yesButton: UIButton!
    
    @IBOutlet weak var noButton: UIButton!
    
    @IBOutlet weak var highPriorityButton: UIButton!
    @IBOutlet weak var mediumPriorityButton: UIButton!
    @IBOutlet weak var lowPriorityButton: UIButton!
    
    var significance: String = ""
    var question: [SelectedQuestions] = []
    var questionProtocol: QuestionProtocol?
    var answer: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }

    func setupController() {
        self.hideKeyboardWhenTappedAround()

        self.dateField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        self.dateField.delegate = self
        commentTextView.text = "Enter the text..."
        commentTextView.textColor = UIColor.lightGray
        
        commentView.layer.borderColor = UIColor.black.cgColor
        commentView.layer.borderWidth = 1
        commentTextView.delegate = self
}
    
    
    @objc func tapDone() {
        if let datePicker = self.dateField.inputView as? UIDatePicker {
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "dd.MM.yyyy"
            self.dateField.text = dateformatter.string(from: datePicker.date)
        }
        self.dateField.resignFirstResponder() // 2-5
    }
    
    
    
    @IBAction func answerAction(_ sender: UIButton) {
        if sender.tag == 1001 {
            sender.alpha = 0
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.yesButton.alpha = 1
                sSelf.yesButton.backgroundColor = .black
                sSelf.yesButton.setTitleColor(.white, for: .normal)
                sSelf.noButton.backgroundColor = .white
                sSelf.noButton.setTitleColor(.black, for: .normal)
                sSelf.answer = "2"
            }
        } else {
            sender.alpha = 0
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.noButton.alpha = 1
                sSelf.noButton.backgroundColor = .black
                sSelf.noButton.setTitleColor(.white, for: .normal)
                sSelf.yesButton.backgroundColor = .white
                sSelf.yesButton.setTitleColor(.black, for: .normal)
                sSelf.answer = "3"
            }
        }
    }
    
    
    @IBAction func significantAction(_ sender: UIButton) {
        switch sender.tag {
        case 1001:
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.significance = "high"
                sSelf.highPriorityButton.backgroundColor = .systemRed
                sSelf.mediumPriorityButton.backgroundColor = .white
                sSelf.lowPriorityButton.backgroundColor = .white
            }
        case 1002:
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.significance = "medium"
                sSelf.mediumPriorityButton.backgroundColor = .yellow
                sSelf.highPriorityButton.backgroundColor = .white
                sSelf.lowPriorityButton.backgroundColor = .white
            }
        case 1003:
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.significance = "low"
                sSelf.lowPriorityButton.backgroundColor = .green
                sSelf.highPriorityButton.backgroundColor = .white
                sSelf.mediumPriorityButton.backgroundColor = .white
            }
        default:
            print("default")
        }
    }

    @IBAction func saveAction(_ sender: Any) {
        if answer == "0" {
            errorAlert(message: "Please choose answer")
        } else {
            var commentForQuestion: String = ""
            guard let date = dateField.text, !date.isEmpty else {
                errorAlert(message: "Please enter the date")
                self.addBorder(view: dateField)
                return }
            guard let answer = answer else { return }
    
            if commentTextView.text == "Enter the text..." {
                commentForQuestion = ""
            } else {
                commentForQuestion = commentTextView.text
            }
            
            for question in question {
                question.sifnificant = significance
                question.answer = answer
                question.comment = commentForQuestion
                question.dateOfInspection = date
            }
            
            for question in question {
                print(question.section)
                print("Row: \(question.row)")
            }
        questionProtocol?.saveArrayOfQuestions(array: question)
            self.dismiss(animated: true)
    }
}
    
    func errorAlert(message: String) {
        let alert = UIAlertController(title: "Incorrect answer", message: message , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
        self.present(alert, animated: true)
    }
    
    
    
    
    
    
}

extension AnswerForQuestionArrayVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.dateField.layer.borderWidth = 0
        }
        
    }
}



extension AnswerForQuestionArrayVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if commentTextView.textColor == UIColor.lightGray {
            commentTextView.text = nil
            commentTextView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if commentTextView.text.isEmpty {
            commentTextView.text = "Enter the text..."
            commentTextView.textColor = UIColor.lightGray
        }
    }
}
