//
//  AnswersArrayXib.swift
//  Lonia
//
//  Created by SSR Lab on 6.04.22.
//

import Foundation
import UIKit


class ArrayAnswerView: UIView {
    
    @IBOutlet weak var dateField: UITextField!
    
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var commentView: UIView!
    
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var saveAction: UIButton!
    
    var questionProtocol: QuestionProtocol?
    
    var answer: String?
    
    var question: [SelectedQuestions] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setupController() {

        self.dateField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        
        commentTextView.text = "Enter the text..."
        commentTextView.textColor = UIColor.lightGray
        
        commentView.layer.borderColor = UIColor.black.cgColor
        commentView.layer.borderWidth = 1
        commentTextView.delegate = self
}
    
    
    @objc func tapDone() {
        if let datePicker = self.dateField.inputView as? UIDatePicker {
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "dd.MM.yyyy"
            self.dateField.text = dateformatter.string(from: datePicker.date)
        }
        self.dateField.resignFirstResponder() // 2-5
    }
    
    
    
    @IBAction func answerAction(_ sender: UIButton) {
        if sender.tag == 1001 {
            sender.alpha = 0
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.yesButton.alpha = 1
                sSelf.yesButton.backgroundColor = .black
                sSelf.yesButton.setTitleColor(.white, for: .normal)
                sSelf.noButton.backgroundColor = .white
                sSelf.noButton.setTitleColor(.black, for: .normal)
                sSelf.answer = "2"
            }
        } else {
            sender.alpha = 0
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.noButton.alpha = 1
                sSelf.noButton.backgroundColor = .black
                sSelf.noButton.setTitleColor(.white, for: .normal)
                sSelf.yesButton.backgroundColor = .white
                sSelf.yesButton.setTitleColor(.black, for: .normal)
                sSelf.answer = "3"
            }
        }
    }

    @IBAction func saveAction(_ sender: Any) {
        if answer == "0" {
        } else {
            var commentForQuestion: String = ""
            guard let date = dateField.text, !date.isEmpty else {
                return }
            guard let answer = answer else { return }
    
            if commentTextView.text == "Enter the text..." {
                commentForQuestion = ""
            } else {
                commentForQuestion = commentTextView.text
            }
            
            for question in question {
                question.answer = answer
                question.comment = commentForQuestion
                question.dateOfInspection = date
            }
            
            for question in question {
                print(question.section)
                print("Row: \(question.row)")
            }
        questionProtocol?.saveArrayOfQuestions(array: question)
        
    }
}

    
    
    
    
    
    
}



extension ArrayAnswerView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if commentTextView.textColor == UIColor.lightGray {
            commentTextView.text = nil
            commentTextView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if commentTextView.text.isEmpty {
            commentTextView.text = "Enter the text..."
            commentTextView.textColor = UIColor.lightGray
        }
    }
}


  
