//
//  InformationVC.swift
//  Lonia
//
//  Created by SSR Lab on 29.03.22.
//

import UIKit

class InformationVC: UIViewController {
    
    
    @IBOutlet weak var informationTextView: UITextView!
    
    var information = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    func setupController() {
        self.informationTextView.text = information
    }


}
