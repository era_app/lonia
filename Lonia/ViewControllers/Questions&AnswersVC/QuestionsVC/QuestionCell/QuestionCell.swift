//
//  QuestionCell.swift
//  Lonia
//
//  Created by SSR Lab on 24.03.22.
//

import UIKit

class QuestionCell: UITableViewCell {
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var selectedButton: UIButton!
    
    @IBOutlet weak var questionLeading: NSLayoutConstraint!
    @IBOutlet weak var labelLeading: NSLayoutConstraint!
    
    @IBOutlet weak var selectedView: UIButton!
    
    var isSelectedMode: Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    func setupCell() {
        self.selectionStyle = .none
        self.selectedButton.layer.borderWidth = 1
        self.selectedButton.layer.borderColor = UIColor.black.cgColor
        self.selectedButton.backgroundColor = .white
        self.selectedButton.layer.cornerRadius = self.selectedButton.frame.height / 2
    }
    
    func setupSelectedView(question: [SelectedQuestions]) {
        if question.count == 0 {
            selectedButton.backgroundColor = .white
        } else {
            selectedButton.backgroundColor = .black
        }
    }
    
    func setupSelected() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.selectedButton.alpha = 1
            sSelf.questionLeading.constant = 10
            sSelf.labelLeading.constant = 0
            sSelf.selectedView.isHidden = false
            sSelf.contentView.layoutIfNeeded()
        }
    }
    
    func setupUnSelected() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.selectedButton.alpha = 0
            sSelf.questionLeading.constant = -30
            sSelf.labelLeading.constant = 10
            sSelf.selectedView.isHidden = true
            sSelf.contentView.layoutIfNeeded()
        }
    }
    
    
}
