//
//  QuestionsCell.swift
//  Lonia
//
//  Created by SSR Lab on 24.03.22.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    func setupCell() {
        categoryLabel.font = UIFont.systemFont(ofSize: 20, weight: .light)
        categoryImage.image = UIImage(systemName: "list.triangle")
        categoryImage.tintColor = UIColor.black
    }
}
