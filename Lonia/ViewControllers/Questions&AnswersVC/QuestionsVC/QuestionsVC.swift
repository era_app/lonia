//
//  QuestionsVC.swift
//  Lonia
//
//  Created by SSR Lab on 24.03.22.
//

import UIKit

protocol QuestionProtocol {
    func saveAnswer(section: Int, indexPath: Int, questionID: String, comment: String, answer: String, images: [String], dateOfInspection: String, isAnswered: Bool, significant: String)
    func saveNote(oldText: String, newText: String, isEdit: Bool)
    func saveArrayOfQuestions(array: [SelectedQuestions])
}

enum ContentType {
    case NotAnswered
    case Answered
}

class QuestionsVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var answerOnArrayButton: UIButton!
    @IBOutlet weak var selectedModeButton: UIButton!
    @IBOutlet weak var noteButton: UIButton!
    
    var tableViewData = [CategorySectionCellData]()
    
    var currentBriefcase: SavedBrifcase!
    
    var contentType: ContentType = .NotAnswered
    
    var selectedQuestion: [SelectedQuestions] = []
    
    @IBOutlet weak var noteTrailing: NSLayoutConstraint!
    var isSelectedMode: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCellData()
        setupController()
        
    }
    
    func setupCellData() {
        var isAnswered: Bool = false
        if contentType == .NotAnswered {
            isAnswered = false
        } else {
            isAnswered = true
            
        }
        
        tableViewData = []
        
        for category in currentBriefcase.categories {
            let data = CategorySectionCellData(opened: false, title: category, sectionsData: currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == isAnswered}))
            if currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == isAnswered}).count == 0 {
                print(category)
            } else {
                tableViewData.append(data)
            }
        }
    }
    
    func setupController() {
        self.title = "Categories"
        
        answerOnArrayButton.isHidden = true
        
        tableView.setupDelegateData(self)
        tableView.registerCell(CategoryCell.self)
        tableView.registerCell(QuestionCell.self)
        
        if contentType == .NotAnswered {
            let notesButton = UIBarButtonItem(title: "Notes", style: .plain, target: self, action: #selector(openNote))
            navigationItem.rightBarButtonItems = [notesButton]
        } else {
            let filters = UIBarButtonItem(title: "Filters", style: .plain, target: self, action: #selector(filtersAction))
            navigationItem.rightBarButtonItems = [filters]
            self.selectedModeButton.isHidden = true
            self.noteButton.isHidden = true
        }
    }
    
    
    @objc func openNote() {
        let vc = NotesVC(nibName: "NotesVC", bundle: nil)
        vc.currentBriefcase = self.currentBriefcase
        vc.notes = self.currentBriefcase.notes
        vc.noteProtocol = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func filtersAction() {
        let alert = UIAlertController(title: "Filters", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Negative answers", style: .default, handler: { [weak self] _ in
            guard let sSelf = self else { return }
            print(sSelf)
            sSelf.tableViewData = []
            for category in sSelf.currentBriefcase.categories {
                let data = CategorySectionCellData(opened: false, title: category, sectionsData: sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true && $0.answer == "3"}))
                if sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true}).count == 0 {
                    print(category)
                } else {
                    sSelf.tableViewData.append(data)
                }
            }
            sSelf.tableView.reloadData()
            
            print("Negative answers")
        }))
        
        alert.addAction(UIAlertAction(title: "Positive answers", style: .default, handler: { [weak self] _ in
            guard let sSelf = self else { return }
            print(sSelf)
            sSelf.tableViewData = []
            for category in sSelf.currentBriefcase.categories {
                let data = CategorySectionCellData(opened: false, title: category, sectionsData: sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true && $0.answer == "2"}))
                if sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true}).count != 0 {
                    sSelf.tableViewData.append(data)
                }
            }
            sSelf.tableView.reloadData()
            print("Positive answers")
        }))
        
        alert.addAction(UIAlertAction(title: "Answers with comments", style: .default, handler: { [weak self] _ in
            guard let sSelf = self else { return }
            print(sSelf)
            sSelf.tableViewData = []
            for category in sSelf.currentBriefcase.categories {
                let data = CategorySectionCellData(opened: false, title: category, sectionsData: sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true && $0.comment != ""}))
                if sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true}).count != 0 {
                    sSelf.tableViewData.append(data)
                }
            }
            sSelf.tableView.reloadData()
            print("Answers with comments")
        }))
        
        alert.addAction(UIAlertAction(title: "Answers without comments", style: .default, handler: { [weak self] _ in
            guard let sSelf = self else { return }
            print(sSelf)
            sSelf.tableViewData = []
            for category in sSelf.currentBriefcase.categories {
                let data = CategorySectionCellData(opened: false, title: category, sectionsData: sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true && $0.comment.replacingOccurrences(of: " ", with: "") == ""}))
                if sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true}).count != 0 {
                    sSelf.tableViewData.append(data)
                }
            }
            sSelf.tableView.reloadData()
            print("Answers without comments")
        }))
        
        
        alert.addAction(UIAlertAction(title: "Answers without attachments", style: .default, handler: { [weak self] _ in
            guard let sSelf = self else { return }
            print(sSelf)
            sSelf.tableViewData = []
            for category in sSelf.currentBriefcase.categories {
                let data = CategorySectionCellData(opened: false, title: category, sectionsData: sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true && $0.images == []}))
                if sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true}).count != 0 {
                    sSelf.tableViewData.append(data)
                }
            }
            sSelf.tableView.reloadData()
            print("Answers with attachments")
        }))
        
        alert.addAction(UIAlertAction(title: "Answers with attachments", style: .default, handler: { [weak self] _ in
            guard let sSelf = self else { return }
            print(sSelf)
            sSelf.tableViewData = []
            for category in sSelf.currentBriefcase.categories {
                let data = CategorySectionCellData(opened: false, title: category, sectionsData: sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true && $0.images != []}))
                if sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true}).count != 0 {
                    sSelf.tableViewData.append(data)
                }
            }
            sSelf.tableView.reloadData()
            print("Answers without attachments")
        }))
        
        
        
        alert.addAction(UIAlertAction(title: "All answers", style: .default, handler: { [weak self] _ in
            guard let sSelf = self else { return }
            print(sSelf)
            sSelf.tableViewData = []
            
            for category in sSelf.currentBriefcase.categories {
                let data = CategorySectionCellData(opened: false, title: category, sectionsData: sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true}))
                if sSelf.currentBriefcase.questions.filter({$0.categoryName == category && $0.isAnswered == true}).count != 0 {
                    sSelf.tableViewData.append(data)
                }
            }
            sSelf.tableView.reloadData()
            print("Answers with attachments")
        }))
        
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { [weak self] _ in
            guard let sSelf = self else { return }
            print(sSelf)
            print("Cancel")
        }))
        
        
        DispatchQueue.main.async { [weak self] in
            guard let sSelf = self else { return }
            if let popoverController = alert.popoverPresentationController {
                print(popoverController)
                sSelf.addActionSheetForiPad(actionSheet: alert)
                sSelf.present(alert, animated: true, completion: nil)
            } else {
            sSelf.present(alert, animated: true)
            }
        }
        
    }
    
    
    
    @IBAction func selectedModeAction(_ sender: Any) {
        if isSelectedMode {
            self.answerOnArrayButton.isHidden = true
            isSelectedMode = false
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.noteTrailing.constant = 15
                sSelf.view.layoutIfNeeded()
            }
        } else {
            isSelectedMode = true
            self.answerOnArrayButton.isHidden = false
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.noteTrailing.constant = -60
                sSelf.view.layoutIfNeeded()
            }
        }
        self.tableView.reloadData()
    }
    
    
    @IBAction private func addButtonPressed(_ sender: UIBarButtonItem) {
        
    }
    
    
    
    
    @IBAction func addNoteAction(_ sender: UIButton) {
        let vc = NewEditNoteVC(nibName: "NewEditNoteVC", bundle: nil)
        vc.contentType = .new
        vc.noteProtocol = self
        navigationController?.present(vc, animated: true)
    }
    
    
    @IBAction func completeQuestionsArrayAction(_ sender: Any) {
        if self.selectedQuestion.count != 0 {
            let vc = AnswerForQuestionArrayVC(nibName: "AnswerForQuestionArrayVC", bundle: nil)
            vc.question = self.selectedQuestion
            vc.questionProtocol = self
            navigationController?.present(vc, animated: true)
        }
    }
    
    
}

extension QuestionsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            tableViewData[indexPath.section].opened = !tableViewData[indexPath.section].opened
            tableView.reloadSections([indexPath.section], with: .automatic)
        } else {
            if isSelectedMode {
                let currentQuestion = self.selectedQuestion.filter({$0.question == tableViewData[indexPath.section].sectionsData[indexPath.row - 1].question})
                if currentQuestion.count == 0 {
                    let question = tableViewData[indexPath.section].sectionsData[indexPath.row - 1]
                    self.selectedQuestion.append(SelectedQuestions(section: indexPath.section, row: indexPath.row, question: question.question, answer: "0", dateOfInspection: "0", comment: "", significant: question.significance))
                } else {
                    var index = 0
                    for que in selectedQuestion {
                        index += 1
                        if que.question == tableViewData[indexPath.section].sectionsData[indexPath.row - 1].question {
                            self.selectedQuestion.remove(at: index - 1)
                        }
                    }
                    
                }
                self.tableView.reloadData()
            } else {
                let question = tableViewData[indexPath.section].sectionsData[indexPath.row - 1]
                let vc = CurrentQuestionVC(nibName: "CurrentQuestionVC", bundle: nil)
                vc.question = question.question
                vc.questionID = question.questionID
                vc.dateOfInspection = question.dateOfInspection
                vc.comment = question.comment
                vc.information = question.questionInformation
                vc.answer = question.answer
                vc.images = question.images
                vc.notes = self.currentBriefcase.notes
                vc.significance = question.significance
                vc.indexPath = indexPath.row
                vc.section = indexPath.section
                vc.questionProtocol = self
                navigationController?.present(vc, animated: true)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CategoryCell.self), for: indexPath)
            guard let categoryCell  = cell as? CategoryCell else {return cell}
            categoryCell.categoryLabel.text = tableViewData[indexPath.section].title
            return categoryCell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: QuestionCell.self), for: indexPath)
            guard let questionCell  = cell as? QuestionCell else {return cell}
            questionCell.questionLabel.text = tableViewData[indexPath.section].sectionsData[indexPath.row - 1].question
            if self.isSelectedMode {
                let favorite =  self.selectedQuestion.filter({$0.question == tableViewData[indexPath.section].sectionsData[indexPath.row - 1].question})
                questionCell.setupSelectedView(question: favorite)
                questionCell.setupSelected()
            } else {
                questionCell.setupUnSelected()
            }
            return questionCell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = tableViewData[section]
        if section.opened {
            return section.sectionsData.count + 1
        } else {
            return 1
        }
    }
    
}


extension QuestionsVC: QuestionProtocol {
    func saveNote(oldText: String, newText: String, isEdit: Bool) {
        DispatchQueue.global().async { [weak self] in
            guard let sSelf = self else { return }
            var briefcasesWithoutCurrent: [SavedBrifcase] = []
            if let briefcase = UserDefaults.standard.data(forKey: "questionUserDefaults") {
                do {
                    let decoder = JSONDecoder()
                    let briefcases = try decoder.decode([SavedBrifcase].self, from: briefcase)
                    briefcasesWithoutCurrent = briefcases.filter({$0.dateOfCreation != sSelf.currentBriefcase.dateOfCreation})
                } catch {
                    print("Unable to Encode Note (\(error))")
                }
            }
            
            if !isEdit {
                guard let currentBriefcase = sSelf.currentBriefcase else { return }
                
                currentBriefcase.notes.append(newText)
                
                briefcasesWithoutCurrent.append(currentBriefcase)
                
                do {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(briefcasesWithoutCurrent)
                    UserDefaults.standard.set(data, forKey: "questionUserDefaults")
                } catch {
                    print("Unable to Encode Note (\(error))")
                }
            } else {
                var index = 0
                for note in sSelf.currentBriefcase.notes {
                    index += 1
                    if note == oldText {
                        sSelf.currentBriefcase.notes.remove(at: index - 1)
                        sSelf.currentBriefcase.notes.append(newText)
                    }
                }
                briefcasesWithoutCurrent.append(sSelf.currentBriefcase)
                do {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(briefcasesWithoutCurrent)
                    UserDefaults.standard.set(data, forKey: "questionUserDefaults")
                } catch {
                    print("Unable to Encode Note (\(error))")
                }
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let sSelf = self else { return }
                sSelf.tableView.reloadData()
            }
        }
    }
    
    func saveAnswer(section: Int, indexPath: Int, questionID: String, comment: String, answer: String, images: [String], dateOfInspection: String, isAnswered: Bool, significant: String) {
        DispatchQueue.global().async { [weak self] in
            guard let sSelf = self else { return }
            
            var briefcasesWithoutCurrent: [SavedBrifcase] = []
            if let briefcase = UserDefaults.standard.data(forKey: "questionUserDefaults") {
                do {
                    let decoder = JSONDecoder()
                    let briefcases = try decoder.decode([SavedBrifcase].self, from: briefcase)
                    briefcasesWithoutCurrent = briefcases.filter({$0.dateOfCreation != sSelf.currentBriefcase.dateOfCreation})
                } catch {
                    print("Unable to Encode Note (\(error))")
                }
            }
            
            guard let question = sSelf.currentBriefcase.questions.filter({$0.questionID == questionID}).first else { return }
            
            question.comment = comment
            question.isAnswered = isAnswered
            question.images = images
            question.dateOfInspection = dateOfInspection
            question.answer = answer
            question.significance = significant
            print("Significant\(significant)")
            briefcasesWithoutCurrent.append(sSelf.currentBriefcase)
            
            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode(briefcasesWithoutCurrent)
                UserDefaults.standard.set(data, forKey: "questionUserDefaults")
            } catch {
                print("Unable to Encode Note (\(error))")
            }
            
            
            if isAnswered && sSelf.contentType == .NotAnswered {
                sSelf.tableViewData[section].sectionsData.remove(at: indexPath - 1)
                if sSelf.tableViewData[section].sectionsData.count == 0 {
                    sSelf.tableViewData.remove(at: section)
                }
                
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let sSelf = self else { return }
                sSelf.tableView.reloadData()
            }
            
        }
    }
    
    func saveArrayOfQuestions(array: [SelectedQuestions]) {
        DispatchQueue.global().async { [weak self] in
            guard let sSelf = self else { return }
            var briefcasesWithoutCurrent: [SavedBrifcase] = []
            if let briefcase = UserDefaults.standard.data(forKey: "questionUserDefaults") {
                do {
                    let decoder = JSONDecoder()
                    let briefcases = try decoder.decode([SavedBrifcase].self, from: briefcase)
                    briefcasesWithoutCurrent = briefcases.filter({$0.dateOfCreation != sSelf.currentBriefcase.dateOfCreation})
                } catch {
                    print("Unable to Encode Note (\(error))")
                }
            }
            
            for questionInArray in array.sorted(by: {$0.row > $1.row}) {
                guard let questionCurrent =  sSelf.currentBriefcase.questions.filter({$0.question == questionInArray.question}).first else
                { return }
                questionCurrent.answer = questionInArray.answer
                questionCurrent.dateOfInspection = questionInArray.dateOfInspection
                questionCurrent.comment = questionInArray.comment
                questionCurrent.significance = questionInArray.sifnificant
                questionCurrent.isAnswered = true
                print(questionInArray.row)
                if  sSelf.contentType == .NotAnswered {
                    sSelf.tableViewData[questionInArray.section].sectionsData.remove(at: questionInArray.row - 1)
                    if sSelf.tableViewData[questionInArray.section].sectionsData.count == 0 {
                        sSelf.tableViewData.remove(at: questionInArray.section)
                    }
                    
                }
            }
            
            briefcasesWithoutCurrent.append(sSelf.currentBriefcase)
            
            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode(briefcasesWithoutCurrent)
                UserDefaults.standard.set(data, forKey: "questionUserDefaults")
            } catch {
                print("Unable to Encode Note (\(error))")
            }
            
            
            sSelf.selectedQuestion = []
            DispatchQueue.main.async { [weak self] in
                guard let sSelf = self else { return }
                sSelf.isSelectedMode = false
                sSelf.tableView.reloadData()
                
            }
            
        }
    }
    
    
    
}
