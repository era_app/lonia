//
//  NotesVC.swift
//  Lonia
//
//  Created by SSR Lab on 1.04.22.
//

import UIKit
protocol UpdateNotes {
    func updateNotes(notes: [String])
}

class NotesVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var notes: [String] = []
    var currentBriefcase: SavedBrifcase!
    var addNote: AddNote?
    var noteProtocol: QuestionProtocol?
    var isAddedNote: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    func setupController() {
        self.title = "Notes"
        tableView.registerCell(NoteCell.self)
        tableView.setupDelegateData(self)
    }
}

extension NotesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isAddedNote {
            addNote?.addNote(note: notes[indexPath.row])
            self.dismiss(animated: true)
        }
        let vc = NewEditNoteVC(nibName: "NewEditNoteVC", bundle: nil)
        vc.contentType = .edit
        vc.editNoteText = notes[indexPath.row]
        vc.oldText = notes[indexPath.row]
        vc.updatedNotes = notes
        vc.updateNotes = self
        vc.noteProtocol = noteProtocol
        navigationController?.present(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NoteCell.self), for: indexPath)
        guard let noteCell  = cell as? NoteCell else {return cell}
        noteCell.noteLabel.text = notes[indexPath.row]
        return noteCell
    }
    
    
    
}

extension NotesVC: UpdateNotes {
    func updateNotes(notes: [String]) {
        self.notes = notes
        tableView.reloadData()
    }
    
    
}
