//
//  New&EditVC.swift
//  Lonia
//
//  Created by SSR Lab on 1.04.22.
//

import UIKit

enum NoteContentType {
    case new
    case edit
}

class NewEditNoteVC: UIViewController {
    
    @IBOutlet weak var noteMainLabel: UILabel!
    
    @IBOutlet weak var noteTextView: UITextView!
    
    var contentType: NoteContentType = .new
    var noteProtocol: QuestionProtocol?
    var updateNotes: UpdateNotes?
    var editNoteText: String = ""
    
    var oldText: String = ""
    
    var updatedNotes: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    func setupController() {
        
        noteTextView.delegate = self
        
        if contentType == .new {
            noteMainLabel.text = "New note"
            noteTextView.text = "Enter the text..."
            noteTextView.textColor = UIColor.lightGray
        } else {
            noteMainLabel.text = "Edit note"
//            self.oldText = editNoteText
            noteTextView.text = editNoteText
            noteTextView.textColor = UIColor.black
        }
        
        
        
        
        
    }
    
    
    @IBAction func saveNoteAction(_ sender: Any) {
        if noteTextView.text.replacingOccurrences(of: " ", with: "") != "Enterthetext..." {
            guard let text = noteTextView.text, !text.isEmpty else {
                self.addBorder(view: noteTextView)
                return }
            if contentType == .new {
                if noteTextView.text.replacingOccurrences(of: " ", with: "") != "Enterthetext..." {
                    noteProtocol?.saveNote(oldText: "", newText: text, isEdit: false)
                    self.dismiss(animated: true)
                }
            } else {
                noteProtocol?.saveNote(oldText: oldText, newText: text, isEdit: true)
                var index = 0
                for note in updatedNotes {
                    index += 1
                    if note == oldText {
                        updatedNotes.remove(at: index - 1)
                        updatedNotes.append(text)
                    }
                }
                updateNotes?.updateNotes(notes: updatedNotes)
                self.dismiss(animated: true)
            }
        }
    }
    
}

extension NewEditNoteVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if noteTextView.text == "Enter the text..." {
            noteTextView.text = nil
            noteTextView.textColor = UIColor.black
        } else if self.editNoteText != "" {
            noteTextView.text = editNoteText
            noteTextView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if noteTextView.text.isEmpty {
            if editNoteText != "" {
                noteTextView.text = editNoteText
                noteTextView.textColor = UIColor.black
            } else {
                noteTextView.text = "Enter the text..."
                noteTextView.textColor = UIColor.lightGray
            }
            
        }
    }
}
