//
//  CurrentQuestionCell.swift
//  Lonia
//
//  Created by Ilya Rabyko on 5.05.22.
//

import UIKit

class CurrentQuestionCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var dateOfInspectionField: UITextField!
    @IBOutlet weak var commentTextView: UITextView!
    
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var dateFieldView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    func setupUI() {
        
        self.selectionStyle = .none
        
        commentTextView.delegate = self
        commentTextView.text = "Enter the text..."
        commentTextView.textColor = UIColor.lightGray
        
        commentView.layer.borderColor = UIColor.black.cgColor
        commentView.layer.borderWidth = 1
        
        dateFieldView.layer.borderColor = UIColor.black.cgColor
        dateFieldView.layer.borderWidth = 1
    }
    
    func setupCelll(question: QuestionsSave) {
        questionLabel.text = question.question
        dateOfInspectionField.text = question.dateOfInspection
        
    }
    
}


extension CurrentQuestionCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if commentTextView.text == "Enter the text..." {
            commentTextView.text = nil
            commentTextView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if commentTextView.text.isEmpty {
            commentTextView.text = "Enter the text..."
            commentTextView.textColor = UIColor.lightGray
        }
    }
}
