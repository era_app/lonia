//
//  CurrentQuestionVC.swift
//  Lonia
//
//  Created by SSR Lab on 26.03.22.
//

import UIKit

protocol AddNote {
    func addNote(note: String)
}

class CurrentQuestionVC: UIViewController {
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var inspectionDataField: UITextField!
    
    @IBOutlet weak var commentTextView: UITextView!
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var dateView: UIView!
    
    
    @IBOutlet weak var informationButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    
    @IBOutlet weak var highPriorityButton: UIButton!
    @IBOutlet weak var mediumPriorityButton: UIButton!
    @IBOutlet weak var lowPriorityButton: UIButton!
    @IBOutlet weak var addPhoto: UIButton!
    
    var questionProtocol: QuestionProtocol?
    
    var questionID: String = ""
    var question: String = ""
    var section: Int = 0
    var indexPath: Int = 0
    var answer: String?
    var comment: String = ""
    var information: String = ""
    var dateOfInspection: String!
    var significance: String = ""
    var images: [String] = []
    var notes: [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    func setupController() {
        self.addBlackBorder(view: addPhoto)
        self.addBlackBorder(view: yesButton)
        self.addBlackBorder(view: noButton)
        self.addBlackBorder(view: highPriorityButton)
        self.addBlackBorder(view: mediumPriorityButton)
        self.addBlackBorder(view: lowPriorityButton)
        
        questionLabel.text = question
        
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        imageCollectionView.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "ImageCell")
        
        self.hideKeyboardWhenTappedAround()
        
        inspectionDataField.delegate = self
        self.inspectionDataField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        
        
        dateView.layer.borderColor = UIColor.black.cgColor
        dateView.layer.borderWidth = 1
        
        commentView.layer.borderColor = UIColor.black.cgColor
        commentView.layer.borderWidth = 1
        commentTextView.delegate = self
        
        
        if comment != "" {
            self.commentTextView.text = comment
            commentTextView.textColor = UIColor.black
        } else {
            commentTextView.text = "Enter the text..."
            commentTextView.textColor = UIColor.lightGray
        }
        
        if  dateOfInspection != "" {
            self.inspectionDataField.text = dateOfInspection
        }
        
        if  information == "" {
            informationButton.isHidden = true
        }
        
        switch significance {
        case "high":
            self.significance = "high"
            highPriorityButton.backgroundColor = .red
        case "medium":
            self.significance = "medium"
            mediumPriorityButton.backgroundColor = .yellow
        case "low":
            self.significance = "low"
            lowPriorityButton.backgroundColor = .green
        default:
            print("default")
        }
        
        if  answer == "2" {
            yesButton.backgroundColor = .black
            yesButton.setTitleColor(.white, for: .normal)
        } else if answer == "3" {
            noButton.backgroundColor = .black
            noButton.setTitleColor(.white, for: .normal)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if answer == "0"  {
            self.questionProtocol?.saveAnswer(section: section, indexPath: indexPath, questionID: questionID, comment: self.commentTextView.text, answer: "0", images: self.images, dateOfInspection: self.inspectionDataField.text ?? "12345432", isAnswered: false, significant: significance)
        }
    }
    
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func errorAlert(message: String) {
        let alert = UIAlertController(title: "Incorrect answer", message: message , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
        self.present(alert, animated: true)
    }
    
    func addBlackBorder(view: UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.black.cgColor
    }
    
    
    
    @objc func tapDone() {
        if let datePicker = self.inspectionDataField.inputView as? UIDatePicker {
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "dd.MM.yyyy"
            self.inspectionDataField.text = dateformatter.string(from: datePicker.date)
        }
        self.inspectionDataField.resignFirstResponder() // 2-5
    }
    
    
    @IBAction func informationAction(_ sender: Any) {
        let vc = InformationVC(nibName: "InformationVC", bundle: nil)
        vc.information = self.information
        self.present(vc, animated: true)
    }
    
    
    @IBAction func answerAction(_ sender: UIButton) {
        if sender.tag == 1001 {
            sender.alpha = 0
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.yesButton.alpha = 1
                sSelf.yesButton.backgroundColor = .black
                sSelf.yesButton.setTitleColor(.white, for: .normal)
                sSelf.noButton.backgroundColor = .white
                sSelf.noButton.setTitleColor(.black, for: .normal)
                sSelf.answer = "2"
            }
        } else {
            sender.alpha = 0
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.noButton.alpha = 1
                sSelf.noButton.backgroundColor = .black
                sSelf.noButton.setTitleColor(.white, for: .normal)
                sSelf.yesButton.backgroundColor = .white
                sSelf.yesButton.setTitleColor(.black, for: .normal)
                sSelf.answer = "3"
            }
        }
    }
    
    
    @IBAction func significantAction(_ sender: UIButton) {
        switch sender.tag {
        case 1001:
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.significance = "high"
                sSelf.highPriorityButton.backgroundColor = .systemRed
                sSelf.mediumPriorityButton.backgroundColor = .white
                sSelf.lowPriorityButton.backgroundColor = .white
            }
        case 1002:
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.significance = "medium"
                sSelf.mediumPriorityButton.backgroundColor = .yellow
                sSelf.highPriorityButton.backgroundColor = .white
                sSelf.lowPriorityButton.backgroundColor = .white
            }
        case 1003:
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.significance = "low"
                sSelf.lowPriorityButton.backgroundColor = .green
                sSelf.highPriorityButton.backgroundColor = .white
                sSelf.mediumPriorityButton.backgroundColor = .white
            }
        default:
            print("default")
        }
    }
    
    
    
    
    
    @IBAction func saveAction(_ sender: Any) {
        if answer == "0" {
            errorAlert(message: "Please choose answer")
        } else {
            var commentForQuestion: String = ""
            guard let date = inspectionDataField.text, !date.isEmpty else {
                errorAlert(message: "Please enter the date")
                self.addBorder(view: inspectionDataField)
                return }
            guard let answer = answer else { return }
            
            if commentTextView.text == "Enter the text..." {
                commentForQuestion = ""
            } else {
                commentForQuestion = commentTextView.text
            }
            
            self.questionProtocol?.saveAnswer(section: section, indexPath: indexPath,questionID: questionID, comment: commentForQuestion, answer: answer, images: self.images, dateOfInspection: date, isAnswered: true, significant: significance)
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func addNoteAction(_ sender: Any) {
        let vc = NotesVC(nibName: "NotesVC", bundle: nil)
        vc.notes = self.notes
        vc.addNote = self
        vc.isAddedNote = true
        self.present(vc, animated: true)
    }
    
    
    @IBAction func imagePickerBtnAction( sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        DispatchQueue.main.async { [weak self] in
            guard let sSelf = self else { return }
            if let popoverController = alert.popoverPresentationController {
                print(popoverController)
                sSelf.addActionSheetForiPad(actionSheet: alert)
                sSelf.present(alert, animated: true, completion: nil)
            } else {
                sSelf.present(alert, animated: true)
            }
        }
    }
    
    func convertBase64StringToImage (imageBase64String: String) -> UIImage {
        let imageData = Data(base64Encoded: imageBase64String)
        let image = UIImage(data: imageData!)
        return image!
    }
    
    
    
    
}

extension CurrentQuestionVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if commentTextView.text == "Enter the text..." {
            commentTextView.text = nil
            commentTextView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if commentTextView.text.isEmpty {
            commentTextView.text = "Enter the text..."
            commentTextView.textColor = UIColor.lightGray
        }
    }
}

extension CurrentQuestionVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            guard let image = pickedImage.jpegData(compressionQuality: 1) else { return }
            self.images.append(image.base64EncodedString())
            self.imageCollectionView.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension CurrentQuestionVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.inspectionDataField.layer.borderWidth = 0
        }
        
    }
}

extension CurrentQuestionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "Image", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            print("ShowImage")
        }))
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
            self.images.remove(at: indexPath.row)
            self.imageCollectionView.reloadData()
        }))
        self.present(alert, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        let image = convertBase64StringToImage(imageBase64String: images[indexPath.row])
        cell.mainImage.image = image
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/3.0
        let height = width
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}

extension CurrentQuestionVC: AddNote {
    func addNote(note: String) {
        self.commentTextView.textColor = UIColor.black
        self.comment = "noteWasAdded"
        if commentTextView.text.replacingOccurrences(of: " " , with: "") == "Enterthetext..." {
            commentTextView.text = note
        } else {
            guard let text = commentTextView.text else { return }
            commentTextView.text = "\(text) \n\(note)"
        }
    }
    
    
}

