//
//  AnswersVC.swift
//  Lonia
//
//  Created by SSR Lab on 20.03.22.
//

import UIKit
import NVActivityIndicatorView

class AnsweredBriefacasesVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var briefCases: [SavedBrifcase] = []
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBriefcases()
    }
    
    
    func setupBriefcases() {
        DispatchQueue.global().async { [weak self] in
            guard let sSelf = self else { return }
            print(sSelf)
            if let briefcase = UserDefaults.standard.data(forKey: "questionUserDefaults") {
                do {
                    let decoder = JSONDecoder()
                    let briefcases = try decoder.decode([SavedBrifcase].self, from: briefcase)
                    // if sSelf.briefCases.count > briefcases.count || sSelf.briefCases.count < briefcases.count  {
                    DispatchQueue.main.async { [weak self] in
                        guard let sSelf = self else { return }
                        sSelf.briefCases = briefcases.sorted(by: {$0.dateOfCreation > $1.dateOfCreation})
                        sSelf.collectionView.reloadData()
                    }
                    
                } catch {
                    print("Unable to Encode Note (\(error))")
                }
            }
        }
        
    }
    
    
    
    func setupController() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "BriefcaseCell", bundle: nil), forCellWithReuseIdentifier: "BriefcaseCell")
        self.title = "Answers"
        
        self.setupNavigationBar(view: self)
        setupBriefcases()
    }
    
    func startUploading() {
        self.view.isUserInteractionEnabled = false
        indicator.isHidden = false
        indicator.color = UIColor.black
        indicator.type = .ballPulse
        indicator.startAnimating()
    }
    
    func finishUploading() {
        self.view.isUserInteractionEnabled = true
        indicator.isHidden = true
        indicator.stopAnimating()
        let alert = UIAlertController(title: "Success", message: "Success!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
        DispatchQueue.main.async { [weak self] in
            guard let sSelf = self else { return }
            if let popoverController = alert.popoverPresentationController {
                print(popoverController)
                sSelf.addActionSheetForiPad(actionSheet: alert)
                sSelf.present(alert, animated: true, completion: nil)
            } else {
                sSelf.present(alert, animated: true)
            }
        }
    }
    
    
    func showAlertAnswered(currentBriefcase: SavedBrifcase) {
        let alert = UIAlertController(title: "Briefcase was completed", message: "What do you want to do with briefcase?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alert.addAction(UIAlertAction(title: "View answers", style: .default, handler: {[weak self] _ in
            guard let sSelf = self else { return }
            let vc = QuestionsVC(nibName: "QuestionsVC", bundle: nil)
            vc.currentBriefcase = currentBriefcase
            vc.contentType = .Answered
            sSelf.navigationController?.pushViewController(vc, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Send answers and save it local", style: .destructive, handler: { _ in
            self.startUploading()
            NetworkManager.shared.sendAnswersAndBriefcase(briefcase: currentBriefcase) { result in
                switch result {
                    
                case .success(let string):
                    if string == "Success!" {
                        DispatchQueue.global().async { [weak self] in
                            guard let sSelf = self else { return }
                            var briefcasesWithoutCurrent: [SavedBrifcase] = []
                            if let briefcase = UserDefaults.standard.data(forKey: "questionUserDefaults") {
                                do {
                                    let decoder = JSONDecoder()
                                    let briefcases = try decoder.decode([SavedBrifcase].self, from: briefcase)
                                    briefcasesWithoutCurrent = briefcases.filter({$0.dateOfCreation != currentBriefcase.dateOfCreation})
                                } catch {
                                    print("Unable to Encode Note (\(error))")
                                }
                            }
                            currentBriefcase.isSended = true
                            
                            briefcasesWithoutCurrent.append(currentBriefcase)
                            do {
                                let encoder = JSONEncoder()
                                let data = try encoder.encode(briefcasesWithoutCurrent)
                                UserDefaults.standard.set(data, forKey: "questionUserDefaults")
                            } catch {
                                print("Unable to Encode Note (\(error))")
                            }
                            
                            sSelf.briefCases = []
                            sSelf.briefCases = briefcasesWithoutCurrent.sorted(by: {$0.dateOfCreation > $1.dateOfCreation})
                            
                            DispatchQueue.main.async { [weak self] in
                                guard let sSelf = self else { return }
                                print("save it local")
                                sSelf.finishUploading()
                                sSelf.collectionView.reloadData()
                            }
                        }
                    }
                case .failure(_):
                    self.finishUploading()
                    let alert = UIAlertController(title: "Error", message: "Error", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    DispatchQueue.main.async { [weak self] in
                        guard let sSelf = self else { return }
                        if let popoverController = alert.popoverPresentationController {
                            print(popoverController)
                            sSelf.addActionSheetForiPad(actionSheet: alert)
                            sSelf.present(alert, animated: true, completion: nil)
                        } else {
                            sSelf.present(alert, animated: true)
                        }
                    }
                }
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: "Send answers and delete it local", style: .destructive, handler: { _ in
            self.startUploading()
            NetworkManager.shared.sendAnswersAndBriefcase(briefcase: currentBriefcase) { result in
                switch result {
                case .success(let string):
                    if string == "Success!" {
                        DispatchQueue.global().async { [weak self] in
                            guard let sSelf = self else { return }
                            var briefcasesWithoutCurrent: [SavedBrifcase] = []
                            if let briefcase = UserDefaults.standard.data(forKey: "questionUserDefaults") {
                                do {
                                    let decoder = JSONDecoder()
                                    let briefcases = try decoder.decode([SavedBrifcase].self, from: briefcase)
                                    briefcasesWithoutCurrent = briefcases.filter({$0.dateOfCreation != currentBriefcase.dateOfCreation})
                                } catch {
                                    print("Unable to Encode Note (\(error))")
                                }
                            }
                            
                            do {
                                let encoder = JSONEncoder()
                                let data = try encoder.encode(briefcasesWithoutCurrent)
                                UserDefaults.standard.set(data, forKey: "questionUserDefaults")
                            } catch {
                                print("Unable to Encode Note (\(error))")
                            }
                            
                            sSelf.briefCases = []
                            sSelf.briefCases = briefcasesWithoutCurrent.sorted(by: {$0.dateOfCreation > $1.dateOfCreation})
                            
                            DispatchQueue.main.async { [weak self] in
                                guard let sSelf = self else { return }
                                print("deleted")
                                sSelf.finishUploading()
                                sSelf.collectionView.reloadData()
                            }
                            
                        }
                    }
                case .failure(_):
                    self.finishUploading()
                    let alert = UIAlertController(title: "Error", message: "Error", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                    DispatchQueue.main.async { [weak self] in
                        guard let sSelf = self else { return }
                        if let popoverController = alert.popoverPresentationController {
                            print(popoverController)
                            sSelf.addActionSheetForiPad(actionSheet: alert)
                            sSelf.present(alert, animated: true, completion: nil)
                        } else {
                            sSelf.present(alert, animated: true)
                        }
                    }
                }
            }
        }))
        
        
        DispatchQueue.main.async { [weak self] in
            guard let sSelf = self else { return }
            if let popoverController = alert.popoverPresentationController {
                print(popoverController)
                sSelf.addActionSheetForiPad(actionSheet: alert)
                sSelf.present(alert, animated: true, completion: nil)
            } else {
                sSelf.present(alert, animated: true)
            }
        }
    }
    
    
    
    
    @IBAction func createBriefcaseAction(_ sender: Any) {
        let vc = NewBriefcaseParametersVC(nibName: "NewBriefcaseParametersVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}

extension AnsweredBriefacasesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return briefCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showAlertAnswered(currentBriefcase: briefCases[indexPath.row])
        //        if briefCases[indexPath.row].questions.count == briefCases[indexPath.row].questions.filter({$0.isAnswered == true}).count {
        //            showAlertAnswered(currentBriefcase: briefCases[indexPath.row])
        //        } else {
        //            let vc = QuestionsVC(nibName: "QuestionsVC", bundle: nil)
        //            vc.currentBriefcase = briefCases[indexPath.row]
        //            vc.contentType = .Answered
        //            vc.hidesBottomBarWhenPushed = true
        //            navigationController?.pushViewController(vc, animated: true)
        //        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BriefcaseCell", for: indexPath) as! BriefcaseCell
        cell.isAnswers = true
        cell.setupCell(briefCase: briefCases[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: width, height: 350)
    }
    
    
    
}


